

<section class="header-page fade-up header-page-tours">
	<div class="bounce-in animate4"><h2 class="header-pagetitle"><?php echo $page_title; ?></h2></div>
</section>

<div class="divider"><span></span></div>

<!--start page-->
<section id="internalpage">

	<!--start container-->
        
       <?php if(count($products) == 0):?>
        <h2 style="margin:50px 0px; text-align:center;">
            <?php echo lang('no_products');?>
        </h2>
    <?php elseif(count($products) > 0):?>
        
        <div class="row" style="margin-top:20px; margin-bottom:15px">
            
            <div class="span3 pull-right">
                <select class="span3" id="sort_products" onchange="window.location='<?php echo site_url(uri_string());?>/'+$(this).val();" style="float: right;height: 32px;margin-right: 84px;width:250px;height:40px;">
                    <option value=''><?php echo lang('default');?></option>
                    <option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/asc')?' selected="selected"':'';?> value="?by=name/asc"><?php echo lang('sort_by_name_asc');?></option>
                    <option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/desc')?' selected="selected"':'';?>  value="?by=name/desc"><?php echo lang('sort_by_name_desc');?></option>
                    <option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/asc')?' selected="selected"':'';?>  value="?by=price/asc"><?php echo lang('sort_by_price_asc');?></option>
                    <option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/desc')?' selected="selected"':'';?>  value="?by=price/desc"><?php echo lang('sort_by_price_desc');?></option>
                </select>
            </div>
        </div>         
    <div class="container clearfix">
    
<?php
            
            //$itm_cnt = 1;
            foreach($products as $product): ?>
		<div class="grid_6">
        	<!--start archivetour-->
            <div class="archivetour yellow fade-left animate1">
                
                <!--left-->
                <div class="leftarchivetour">
                
                    <!--<a href="single-project.php">-->
                         <?php
                        $photo  = theme_img('no_picture.png', lang('no_image_available'));
                        $product->images    = array_values($product->images);
                        
                        if(!empty($product->images[0]))
                        {
                            $primary    = $product->images[0];
                            foreach($product->images as $photo)
                            {
                                if(isset($photo->primary))
                                {
                                    $primary    = $photo;
                                }
                            }

                            $photo  = '<img  class="imgleftarchivetour opacity" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                        }
                        ?>
                        <a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><?php echo $photo; ?></a>
                        <!--<img alt="" class="imgleftarchivetour opacity" src="<?php echo base_url(); ?>assets/forest/img/tours/photos/img1.jpg">-->
                    <!--</a>-->
                
                    <div class="pricetitleleftarchivetour">
                        
                        <div class="titleleftarchivetour">
                            <?php if($product->saleprice > 0):?>
                                <p><?php echo format_currency($product->price); ?></p>
                                
                            <?php else: ?>
                                <p><?php echo format_currency($product->price); ?></p>
                            <?php endif; ?>
                        </div>
                        <!--<p class="titleleftarchivetour">For person</p>-->
                    </div>
                
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivetour">
                
                    <div class="titledayarchivetour">
                        <!--<a href="single-project.php"><p class="titlearchivetour">BRAZIL</p></a>-->
                         
                             <a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><p class="titlearchivetour"><?php echo $product->name;?></p></a>
                            <?php if($this->session->userdata('admin')): ?>
                                <!--<a class="btn" title="<?php echo lang('edit_product'); ?>" href="<?php echo  site_url($this->config->item('admin_folder').'/products/form/'.$product->id); ?>"><i class="icon-pencil"></i></a>-->
                            <?php endif; ?>
                        
                        <div class="dayarchivetour">
                            <p><?php echo count($product->images); ?></p>
                            <span>DAYS</span>
                        </div>
                    
                    </div>
                    
                    <div class="descriptioniconsarchivetour">
                       
                        <p class="descriptionarchivetour">
                            
                            <?php if($product->description != ''): 
                            
                                if(strlen($product->description)<=120)
                                    {
                                      $y=$product->description;
                                    }
                                    else
                                    {
                                      $y=substr($product->description,0,100) . '...';
                                      
                                    }
                                $y1=str_replace("<p>","",$y);
                                $y2=str_replace("</p>","",$y1);
                                echo $y2;
                                endif; ?>
                        </p>
                        <p class="iconsarchivetour">
                        
                            <a title="Car" class="tooltip" href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/icon/car.png"></a>
                            <a title="Fly and Drive" class="tooltip" href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/icon/plane.png"></a>
                            <a title="Sun" class="tooltip" href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/icon/sun.png"></a>
                            <a title="Insurance included" class="tooltip" href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/icon/insurance.png"></a>
							<a title="Nature" class="tooltip" href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/icon/nature.png"></a>
                        
                        </p>
                    
                    </div>
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archivetour-->
        </div>
        <?php endforeach; ?>
        
        
    
    </div>
        <div class="span9">
                <?php echo $this->pagination->create_links();?>&nbsp;
            </div>
    <!--end container--> 
    <?php endif;?>
</section>
<!--end internal page-->

<div class="divider"><span></span></div>


	<!--Start js-->    
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.min.js"></script> <!--Jquery-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="<?php echo base_url(); ?>assets/forest/js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="<?php echo base_url(); ?>assets/forest/js/scroolto.js"></script> <!--Scrool To-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.inview.min.js"></script> <!--inview-->
    <script src="<?php echo base_url(); ?>assets/forest/js/menu/hoverIntent.js"></script> <!--superfish-->
    <script src="<?php echo base_url(); ?>assets/forest/js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="<?php echo base_url(); ?>assets/forest/js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
    <script src="<?php echo base_url(); ?>assets/forest/js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="<?php echo base_url(); ?>assets/forest/js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			
			$('.header-page').parallax("100%", 0.1);

		});
		//end parallax
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		

		/* ]]> */
	</script>
    
