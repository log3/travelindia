<!--start contact-->
<div class="contactmap">
	
    <!--start info contact-->
<!--    <div class="infocontact infocontactlight blue">
    
    	<div class="contentinfocontact">
        	<p class="titleinfocontact">TRAVEL INDIA</p>
            <ul>
            	<li><p><img alt="" src="img/contact/iconinfocontactaddress.png">Shop 6, Prospect Shopping Village 3</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactaddress.png">Aldgate Street Prospect NSW 2148</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactphone.png">Telephone: +61 2 9631 0206</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactmail.png">Mail:sales@travelairinternational.com</p></li>
            </ul>
        </div>
        
        <div class="triangleinfocontact">
        	<span></span>
        </div>
        
    	start marker
        <div class="markercontactmap">
            <div class="circlemarker"><div class="innercirclemarker"></div></div>
            <div class="trianglemarker"></div>
        </div>
        end marker
    
    </div>-->
    <!--end info contact-->
    
	<!--google maps-->
    <div id="googleMap" style="width:100%;height:550px;margin-top:62px;"></div>
    <!--google maps-->
    	
</div>
<!--end contactmap-->

<div class="divider"><span></span></div>

<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    	
		<!--accordion-->
		<div class="grid_4 violet fade-left">
			<h2 class="titlewithborder"><span>OUR FEATURES</span></h2>
            
			<div class="dividerheight20"></div>
            
            <!--start accordion-->
            <div class="accordion accordionlight">
                
                <h4 class="violet-borderleft firstaccordiontitle">CREATIVE</h4>
                <div>
                    <p>
                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>
                </div>
                
                <h4 class="violet-borderleft">DYNAMIC</h4>
                <div>
                    <p>
                     Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>
                </div>
                
                <h4 class="violet-borderleft">PROFESSIONAL</h4>
                <div>
                    <p>
                     Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>

                </div>
                
            </div>
            <!--end accordion-->
		</div>
		<!--end accordion-->
		
		<!--simple text-->
		<div class="grid_4 blue fade-up">
			<h2 class="titlewithborder"><span>OUR AGENCY</span></h2>
			<div class="dividerheight20"></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><br/>
			<blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc.</blockquote>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit.</p>
            
		</div>
		<!--end simple text-->
		
		<!--start form-->
		<div class="grid_4 green fade-right">
			<h2 class="titlewithborder"><span>CONTACT US</span></h2>
			
			<!--start form-->
                        <div id="success">
                        <span class="green textcenter" style="position:relative;width: none;">
                            <p style="margin-top: 1em;">Your message was sent successfully! We will be in touch as soon as We can.</p>
                            </span>
                        </div>

                        <div id="error">
                            <span>
                                <p>Something went wrong, try refreshing and submitting the form again.</p>
                            </span>
                        </div>

                        <form id="contact" name="contact" method="post" novalidate="novalidate">
                            <fieldset>
                        <!--        <label for="name" id="name">Name<span class="required">*</span>
                                </label>-->
                                <input type="text" name="name" id="name" size="30" value="" required="" placeholder="Name">
                        <!--        <label for="email" id="email">Email<span class="required">*</span>
                                </label>-->
                                <input type="text" name="email" id="email" size="30" value="" required="" placeholder="Email Address">
                                <!--<label for="phone" id="phone">Phone<span class="required">*</span></label>-->
                                <input type="text" name="phone" id="phone" size="30" value="" placeholder="Phone Number">
                        <!--        <label for="Message" id="message">Message<span class="required">*</span>
                                </label>-->
                                <textarea name="message" id="message" required="" placeholder="Message"></textarea>

                                <input id="submit" type="submit" name="submit" value="Send" style="margin-top:15px">
                            </fieldset>
                        </form>

		<!--end form-->
			
		</div>
		<!--end form-->
		
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>



	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script> <!-- Google Map API -->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script src="http://maps.googleapis.com/maps/api/js"></script>

<script>
var myCenter=new google.maps.LatLng(-33.799539, 150.926533);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:5,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
var myCenter=new google.maps.LatLng(-33.799539, 150.926533);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:13,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);

var infowindow = new google.maps.InfoWindow({
  content:"Shop 6, Prospect Shopping Village 3\n\
Aldgate Street Prospect NSW 2148"
  });

infowindow.open(map,marker);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

</body>  
</html>