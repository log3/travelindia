    <!--start footer-->
    <footer id="footer">
        
        <!--start container-->
        <div class="container clearfix">
        
            <div class="grid_3 gridfooter">
                <h3>Why choose us?</h3>
                <ul>
                    <li>Focus on experiencing destinations rather than visiting them</li>
                    <li>Innovative itineraries that take you beyond just the tourist sites</li>
                    <li>Many years experience of safe and hassle-free India travel</li>
                    <li>Enjoy the perfect balance of unique experiences and free time</li>
                    <li>Expert English speaking local guides</li>
                </ul>   
            </div>
    
            <div class="grid_3 gridfooter">
                <h3>Contacts</h3>
                <p>Address: <br/>Shop 6, Prospect Shopping Village 3<br />Aldgate Street Prospect NSW 2148<br />Phone: +61 2 9631 0206<br />Mail: sales@travelairinternational.com</p>
                <p class="socialfooter"><a href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/footer/facebook.jpg" /></a><a href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/footer/dribble.jpg" /></a><a href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/footer/twitter.jpg" /></a><a href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/footer/instagram.jpg" /></a><a href="#"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/footer/google.jpg" /></a></p>  
            </div>
    
            <div class="grid_3 gridfooter">
                <h3>Tweet</h3>
                
				<div id="tweets"></div>

            </div>
            
            <div class="grid_3 gridfooter">
                <h3>Newsletter</h3>
                <p>Subscribe to our newsletter to get the latest scoop right to your inbox.</p> 
                
                <!--start form-->
                <form class="newsletterfooter">
                    <input type="text" name="newsletter" placeholder="Email Address"/>
                    <input type="submit" />
                </form>
                <!--end form-->
                  
            </div>
        
        </div>
        <!--end container--> 
        
    </footer>
    <!--end footer-->
    
    <!--start copyright-->
    <section id="copyright">
        
        <!--start container-->
        <div class="container">
        
            <div class="grid_12">
                <p>© Copyright 2015 by Travel India - All Rights Reserved</p>   
            </div>
    
        </div>
        <!--end container-->
        
        <div class="backtotop">
        	<a href="#startpage"><img alt="" src="<?php echo base_url(); ?>assets/forest/img/footer/arrowbacktotop.png" /></a>
        </div> 
        
    </section>
    <!--end copyright-->