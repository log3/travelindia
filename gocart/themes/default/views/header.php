
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <meta charset="utf-8">  
    
    <title>Travel India</title> <!--insert your title here-->  
   
    
  	<!--START CSS--> 
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/slider/pgwslider.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/slider/pgwslider.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/style.css"> <!--main-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/grid.css"> <!--grid-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/responsive.css"> <!--responsive-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/rs-plugin/css/settings.css" media="screen" /> <!--rev slider-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/showbizpro/css/settings.css" media="screen" /> <!--showbiz-->
    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/animate.css"> animate-->
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/superfish.css" media="screen"> <!--menu-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/fancybox/jquery.fancybox.css"> <!--main fancybox-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/fancybox/jquery.fancybox-thumbs.css"> <!--fancybox thumbs-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/isotope.css"> <!--isotope-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/flexslider.css"> <!--flexslider-->
    <!--END CSS-->
    
    <!--google fonts-->
    <link href='http://fonts.googleapis.com/css?family=Signika:400,300,600,700' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>  
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>  
    <![endif]-->  
    
    <!--FAVICONS-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/forest/img/favicon/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/forest/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/forest/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/forest/img/favicon/apple-touch-icon-114x114.png">
    <!--END FAVICONS-->
    
    <?php //echo theme_css('bootstrap.min.css', true);?>
<?php //echo theme_css('bootstrap-responsive.min.css', true);?>
<?php //echo theme_css('styles.css', true);?>

<?php echo theme_js('jquery.js', true);?>
<?php echo theme_js('bootstrap.min.js', true);?>
<?php echo theme_js('squard.js', true);?>
<?php echo theme_js('equal_heights.js', true);?>
    
    
    
</head>  
<body id="startpage">

<?php //$this->load->view("left-menu.php"); ?>

<!--start header-->
<header id="navigationmenu" class="fade-down animate1 navigationmenulight">
	
    <!--start left menu close-->
<!--    <div class="leftmenuclose">
    	<img alt="" src="<?php echo base_url(); ?>assets/forest/img/header/leftmenuclose.png">
    </div>-->
    <!--end left menu close-->
    
    <!--start container-->
    <div class="container">
    
        <!--start navigation-->
    	<div class="grid_12 gridnavigation">
        
            <a href="<?php echo base_url(); ?>"> <img class="logo fade-up animate4" alt="" src="<?php echo base_url(); ?>assets/forest/img/logo.jpg" height="44px"></a>
            <?php $this->load->view("navigation.php"); ?>	
<!--            <div id="tag_line">
                <p>Memorable tours at the best prices</p>
                <p>we're different, we're better, we're tourific</p>
            </div>-->
<!--            <div>
                <span id="contact_no"><span id="call_phone">&nbsp;</span><span id="call_no">  (02) 9631 0206</span></span>
                     
            </div>-->
        </div>
        <!--end navigation-->

    </div>
    <!--end container-->
    
<!--    <div class="rightsearchclose">
    	<img alt="" src="<?php echo base_url(); ?>assets/forest/img/header/rightsearch.png">
    </div>   -->
    
</header>
<!--end header-->

<?php //$this->load->view("right-search.php"); ?>

