<!--start slide-->
<section id="homedestinations">
	<div class="container arrowscarousel yellow clearfix">
        
        <!--start arrows carousel-->
        <div class="grid_6">
            <div id="showbiz_left_2" class="arrowcarouselprev fade-right"></div>
        </div>
        <div class="grid_6">
            <div id="showbiz_right_2" class="arrowcarouselnext fade-left"></div>
        </div>
        <!--end arrows carousel-->
        
    </div>
	<!--start carousel-->
    
    <div class="container clearfix showbiz-container">
    	<div class="showbiz" data-left="#showbiz_left_2" data-right="#showbiz_right_2" data-play="#showbiz_play_2">
        	<div class="overflowholder">
				<ul>
    
		<?php foreach($products as $product): ?>			
                    <li class="sb-modern-skin">
                    
                        <div class="destinationcarousel single-carousel">
                        	<div class="mediaholder_innerwrap">
                                    <?php
                        $photo  = theme_img('no_picture.png', lang('no_image_available'));
                        $product->images    = array_values($product->images);
                        
                        if(!empty($product->images[0]))
                        {
                            $primary    = $product->images[0];
                            foreach($product->images as $photo)
                            {
                                if(isset($photo->primary))
                                {
                                    $primary    = $photo;
                                }
                            }

                            $photo  = '<img  class="imgdestination" opacity" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                        }
                        ?>
                                    <?php echo $photo; ?>
                        		<!--<img alt="" src="<?php echo base_url(); ?>assets/forest/img/gallery/masonry/little/img2-vertical.jpg">-->
                        	</div>
                        </div>
                        
                        <div class="darkhover"></div>
                        
                        <div class="detailholder blue">
                        	<div class="showbiz-title"><a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><?php echo $product->name;?></a><span class="bulletgallery"></span></div>
                        	
                            <p class="excerpt">
                            	<?php if($product->description != ''): 
                            
                                if(strlen($product->description)<=120)
                                    {
                                      $y=$product->description;
                                    }
                                    else
                                    {
                                      $y=substr($product->description,0,150) . '...';
                                      
                                    }
                                $y1=str_replace("<p>","",$y);
                                $y2=str_replace("</p>","",$y1);
                                echo $y2;
                                endif; ?>
                        	</p>

                        	<div class="sb-readmore">
                            	<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>">READ MORE</a>
                           	</div>
                        	
                            <div class="sb-clear"></div>
                        </div>
                    
                    </li>
                    <?php endforeach; ?>
                    
               
    			</ul>
    			
                <div class="sbclear"></div>
    		</div>
    		<div class="sbclear"></div>
    	</div>
    </div>
    <!--end carousel-->

    
</section>
<!--end slide-->