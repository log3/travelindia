<!--start services-->
<section id="homeservices" class="sectionhome fade-up">

	<!--start container-->
    <div class="container clearfix">
    
        <!--start 1 service-->
        <div class="grid_4 homeservice">
            
            <div class="imgserviceopen">
            	<img alt="" src="<?php echo base_url(); ?>assets/forest/img/service/service1open.jpg" />
            </div>
            
            <div class="imgservice">
            	<!--start percentagehome-->
                <div class="percentagehome" data-percent="90">
                    <img alt="" src="<?php echo base_url(); ?>assets/forest/img/service/service1.png" />
                </div>
            	<!--end percentagehome-->
            </div>
            
            <h2><a href="#">ADVENTURE</a></h2>
                <p style="text-align: justify;">Safari   - Safari Tours  are a great way to see the Australian outback and get up close with Iconic Aussie destinations like Uluru and Kakada National Park.</p>
            </div>
        <!--end 1 service-->
        
        <!--start 2 service-->
        <div class="grid_4 homeservice">
            
            <div class="imgserviceopen">
            	<img alt="" src="<?php echo base_url(); ?>assets/forest/img/service/service2open.jpg" />
            </div>
            
            <div class="imgservice">
            
            	<!--start percentagehome-->
                <div class="percentagehome" data-percent="80">
                    <img alt="" src="<?php echo base_url(); ?>assets/forest/img/service/service2.png" />
                </div>
            	<!--end percentagehome-->
                
            </div>
            
            <h2><a href="#">RELAX</a></h2>
            <p style="text-align: justify;">Australia is the worlds largest Island with so many sights to explore and enjoy. Whether exploring the traditional lifestyle of the nations aboriginal people.</p>    
        </div>
        <!--end 2 service-->
        
        <!--start 3 service-->
        <div class="grid_4 homeservice">
            
            <div class="imgserviceopen">
            	<img alt="" src="<?php echo base_url(); ?>assets/forest/img/service/service3open.jpg" />
            </div>
            
            <div class="imgservice">
            
            	<!--start percentagehome-->
                <div class="percentagehome" data-percent="70">
                    <img alt="" src="<?php echo base_url(); ?>assets/forest/img/service/service3.png" />
                </div>
            	<!--end percentagehome-->
                
            </div>
            
            <h2><a href="#">HONEYMOON</a></h2>
            <p style="text-align: justify;">Discover the best islands, beaches, resorts, attractions and exciting things to do at each of these top honeymoon destinations to plan your perfect honeymoon.</p>   
        </div>
        <!--end 3 service-->
    
    </div>
    <!--end container--> 
    
</section>
<!--end services-->