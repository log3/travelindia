<!--start hometour-->
<section id="hometours">

	<!--start container-->
    <div class="container clearfix">
    
<!--        <div class="grid_12">
        	<div class="titlesection">
            	<h1>AWESOME TOURS</h1>
            	<h4>LOREM IPSUM DOLOR SIT AMET CONSECTEUR ADIP</h4> 
            </div>  
        </div>-->
        
        <!--start grid-->
        <div class="grid_8">
        
            <!--START HOME TOUR-->
            <div class="hometour hometour-0 blue fade-down animate1">
                
                <!--start tophometour-->
                <div class="tophometour">
                    <div class="titleimghometour">
                        <!--<p class="titlehometour">WEST COAST - USA</p>-->
                        <img alt="" class="imghometour" src="<?php echo base_url(); ?>assets/forest/img/best_tours/ayurved.jpg">
                    </div>
                    <div class="datedescriptionhometour">
                        <img alt="" class="imghometour" src="<?php echo base_url(); ?>assets/forest/img/best_tours/goa-add.jpg">	
                    </div>	
                </div>
                <div class="tophometour">
                    <div class="titleimghometour">
                        <!--<p class="titlehometour">WEST COAST - USA</p>-->
                        <img alt="" class="imghometour" src="<?php echo base_url(); ?>assets/forest/img/best_tours/comm100-offline.png">
                    </div>
                    <div class="datedescriptionhometour">
                        <img alt="" class="imghometour" src="<?php echo base_url(); ?>assets/forest/img/tours/tour1.jpg">	
                    </div>	
                </div>
                <!--end tophometour-->
                
                <!--start bottomhometour-->
<!--                <div class="bottomhometour">
                    
                    start tabs
                    <div class="tabshometour">
                    
                        <div class="hometabs"><ul><li><a title="Weather" href="#tabs-1"><img class="iconhometabsdefault" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/weather.png" /><img class="iconhometabsactive" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/weatheractive.png" /></a></li><li><a title="Price" href="#tabs-2"><img class="iconhometabsdefault" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/money.png" /><img class="iconhometabsactive" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/moneyactive.png" /></a></li><li><a title="Gallery" href="#tabs-3"><img class="iconhometabsdefault" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/gallery.png" /><img class="iconhometabsactive" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/galleryactive.png" /></a></li></ul>
                            <div class="contenthometab weatherhometab" id="tabs-1">
                                <img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/weathertour-blue.png" /><p><strong>RAIN 35 C°</strong></p>
                            </div>
                            <div class="contenthometab moneyhometab" id="tabs-2">
                                <img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/moneytour-blue.png" /><p><strong>2.000 $</strong></p>
                            </div>
                            <div class="contenthometab galleryhometab" id="tabs-3">
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery1.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery1.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery2.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery2.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery3.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery3.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery4.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery4.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery5.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery5.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery6.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery6.jpg" alt="" /></a>
                            </div>
                        </div>
                    
                    </div>
                    end tabs
                    
                    start listhometour
                    <div class="listhometour">
                        <ul>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                        </ul>
                    </div>
                    end listhometour
                    
                    start footer hometour
                    <div class="footerhometour">
                        
                        <div class="contacthometour">
                            <a href="#">
								<span>
									<img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/contacticon.png">
									CONTACT
								</span>
							</a>
                        </div>
                       
                        <div class="morehometour">
                            <a href="#">
								<span>
									<img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/moreicon.png">
									MORE
								</span>
							</a>
                        </div>
    
                    </div>
                    end footer hometour	
                   
                </div>-->
                <!--end bottomhometour-->
                
            </div>
            <!--END HOME TOUR-->
        
        </div>
        <!--end grid-->
        
        
        <!--start grid-->
        <div class="grid_4">
        
            <!--START HOME TOUR-->
            <div class="hometour hometour-1 green fade-down animate2">
                
                <!--start tophometour-->
                <div class="tophometour" >
<!--                    <div class="titleimghometour">
                        <p class="titlehometour">YUCATAN - MEXICO</p>
                        <img alt="" class="imghometour" src="<?php echo base_url(); ?>assets/forest/img/tours/tour2.jpg">
                    </div>-->
<div class="datedescriptionhometour" >
                        <div class="datedayhometour">
                            <p class="datehometour" style="padding: 11px">
                                CONTACT
                            </p>
                            <div class="dayhometour">
                                <p></p>
                                <span><img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/contacticon.png"></span>
                            </div>
                        </div>
                        <div class="descriptionhometour">
                            <p><strong class="titledescriptionhometour">Title description</strong><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla rhoncus ultrices purus, volutpat malesuada sapien sollicitudin vitae.</p>
                        </div>	
                    </div>	
                    <div id="success">
                        <span class="green textcenter" style="position:relative;width: none;">
                            <p style="margin-top: 1em;">Your message was sent successfully! We will be in touch as soon as We can.</p>
                            </span>
                        </div>

                        <div id="error">
                            <span>
                                <p>Something went wrong, try refreshing and submitting the form again.</p>
                            </span>
                        </div>

                        <form id="contact" name="contact" method="post" novalidate="novalidate">
                            <fieldset>
                        <!--        <label for="name" id="name">Name<span class="required">*</span>
                                </label>-->
                                <input type="text" name="name" id="name" size="30" value="" required="" placeholder="Name">
                        <!--        <label for="email" id="email">Email<span class="required">*</span>
                                </label>-->
                                <input type="text" name="email" id="email" size="30" value="" required="" placeholder="Email Address">
                                <!--<label for="phone" id="phone">Phone<span class="required">*</span></label>-->
                                <input type="text" name="phone" id="phone" size="30" value="" placeholder="Phone Number">
                        <!--        <label for="Message" id="message">Message<span class="required">*</span>
                                </label>-->
                                <textarea name="message" id="message" required="" placeholder="Message"></textarea>

                                <input id="submit" type="submit" name="submit" value="Send" style="margin-top:15px">
                            </fieldset>
                        </form>
                </div>
                <!--end tophometour-->
                
                <!--start bottomhometour-->
<!--                <div class="bottomhometour">
                    
                    start tabs
                    <div class="tabshometour">
                    
                        <div class="hometabs"><ul><li><a title="Weather" href="#tabs-4"><img class="iconhometabsdefault" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/weather.png" /><img class="iconhometabsactive" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/weatheractive.png" /></a></li><li><a title="Price" href="#tabs-5"><img class="iconhometabsdefault" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/money.png" /><img class="iconhometabsactive" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/moneyactive.png" /></a></li><li><a title="Gallery" href="#tabs-6"><img class="iconhometabsdefault" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/gallery.png" /><img class="iconhometabsactive" alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/galleryactive.png" /></a></li></ul>
                            <div class="contenthometab weatherhometab" id="tabs-4">
                                <img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/weathertour-green.png" /><p><strong>CLOUDY 35 C°</strong></p>
                            </div>
                            <div class="contenthometab moneyhometab" id="tabs-5">
                                <img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/moneytour-green.png" /><p><strong>2.500 $</strong></p>
                            </div>
                            <div class="contenthometab galleryhometab" id="tabs-6">
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-2" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery1.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery1.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-2" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery2.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery2.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-2" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery3.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery3.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-2" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery4.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery4.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-2" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery5.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery5.jpg" alt="" /></a>
                                <a class="fancybox-thumbs" data-fancybox-group="gallery-2" href="<?php echo base_url(); ?>assets/forest/img/tours/gallery6.jpg"><img src="<?php echo base_url(); ?>assets/forest/img/tours/gallery6.jpg" alt="" /></a>
                            </div>
                        </div>
                    
                    </div>
                    end tabs
                    
                    start listhometour
                    <div class="listhometour">
                        <ul>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                            <li><p>Lorem ipsum dolor sit<span class="bulletlisthometour"></span></p></li>
                        </ul>
                    </div>
                    end listhometour
                    
                    start footer hometour
    
                    <div class="footerhometour">
                        
                        <div class="contacthometour">
                            <a href="#">
								<span>
									<img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/contacticon.png">
									CONTACT
								</span>
							</a>
                        </div>
                       
                        <div class="morehometour">
                            <a href="#">
								<span>
									<img alt="" src="<?php echo base_url(); ?>assets/forest/img/tours/moreicon.png">
									MORE
								</span>
							</a>
                        </div>
    
                    </div>
                    end footer hometour	
                   
                </div>-->
                <!--end bottomhometour-->
                
            </div>
            <!--END HOME TOUR-->
        
        </div>
        <!--end grid-->

    
    </div>
    <!--end container--> 
    
</section>
<!--end hometour-->