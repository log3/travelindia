<?php $this->load->view("header.php"); ?>
<?php $this->load->view("home/slide-full-width.php"); ?>
<?php $this->load->view("home/services.php"); ?>
<!--<div class="divider"><span></span></div>-->


<?php $this->load->view("home/best_tours.php"); ?>
<!--<div class="divider"><span></span></div>-->
<?php $this->load->view("home/tours.php"); ?>
<div class="divider"><span></span></div>
<?php $this->load->view("home/promotions.php"); ?>
<?php $this->load->view("home/destinations.php"); ?>


<div class="divider"><span></span></div>
<?php $this->load->view("home/clients.php"); ?>
<!--<div class="divider"><span></span></div>-->
<?php // $this->load->view("footer.php"); ?>
<?php //print_r($products); ?>
	<!--Start js-->    
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.min.js"></script> <!--Jquery-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="<?php echo base_url(); ?>assets/forest/js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="<?php echo base_url(); ?>assets/forest/rs-plugin/js/jquery.themepunch.plugins.min.js"></script> <!--rev slider-->
    <script src="<?php echo base_url(); ?>assets/forest/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> <!--rev slider-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/forest/showbizpro/js/jquery.themepunch.plugins.min.js"></script> <!--showbiz-->						
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/forest/showbizpro/js/jquery.themepunch.showbizpro.min.js"></script> <!--showbiz-->
    <script src="<?php echo base_url(); ?>assets/forest/js/scroolto.js"></script> <!--Scrool To-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.easy-pie-chart.js"></script> <!--Chart-->
    <script src="<?php echo base_url(); ?>assets/forest/js/fancybox/jquery.fancybox.js"></script> <!--main fancybox-->
    <script src="<?php echo base_url(); ?>assets/forest/js/fancybox/jquery.fancybox-thumbs.js"></script> <!--fancybox thumbs-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.inview.min.js"></script> <!--inview-->
    <script src="<?php echo base_url(); ?>assets/forest/js/menu/hoverIntent.js"></script> <!--superfish-->
    <script src="<?php echo base_url(); ?>assets/forest/js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="<?php echo base_url(); ?>assets/forest/js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="<?php echo base_url(); ?>assets/forest/js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="<?php echo base_url(); ?>assets/forest/js/settings.js"></script> <!--settings-->
    <script src="<?php echo base_url(); ?>assets/forest/slider/pgwslider.js"></script>
    <script src="<?php echo base_url(); ?>assets/forest/slider/pgwslider.min.js"></script>
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		
		
		//start carousel
		jQuery(document).ready(function() {

			jQuery('.showbiz-container').showbizpro({
				dragAndScroll:"on",
				visibleElementsArray:[4,3,2,1]
			});
		   
		});
		//end carousel
		
		
		//start revolution slider
		var revapi;

		jQuery(document).ready(function() {

			   revapi = jQuery('.tp-banner-full-width').revolution(
				{
					delay:9000,
					startwidth:600,
					startheight:370,
					hideThumbs:10,
					navigationType:"none",
					fullWidth:"off",
					forceFullWidth:"off"
				});

		});	//ready
		//end revolution slider
		
		
		//start chart
		$(document).ready(function(){
						
			$('.percentagehome').easyPieChart({
				size: 140,
				rotate: 0,
				lineWidth: 10,
				animate: 1000,
				barColor: '#55738F',
				trackColor: 'transparent',
				scaleColor: false,
				lineCap: 'butt',
			});

		});
		//end chart
		
		//start tour
		$(document).ready(function(){

			var qnthometour = $('.hometour').length;
			
			
			setInterval(function(){
				
				i=0;
				
				while ( i < qnthometour ){

					//title and img hometours height
					var titleimghometourheight = $(".hometour-"+i+" .titleimghometour").height();
					var datedayhometourheight = $(".hometour-"+i+" .datedayhometour").height();
			
					$(".hometour-"+i+" .descriptionhometour").css({
					  "height": titleimghometourheight - datedayhometourheight
					});	
	
					//tabshometourheight
					var tabshometourheight = $(".hometour-"+i+" .tabshometour").height();
					var footerhometourheight = $(".hometour-"+i+" .footerhometour").height();
			
					$(".hometour-"+i+" .listhometour").css({
					  "height": tabshometourheight - footerhometourheight
					});
					
					i++;	
				}
			
			}, 0);
			
		});
		//end tour
		
		
		//start tab and tooltip
		$(document).ready(function() {
			$(".hometabs").tabs();
			$( ".hometabs, .tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tab and tooltip
		
		
		//start scroll
		$(document).ready(function() {
			//description hometour
			$(".descriptionhometour").niceScroll({
				touchbehavior:false,
				cursorcolor:"#EBEEF2",
				cursoropacitymax:0.9,
				cursorwidth:3,
				autohidemode:true,
				cursorborder:"0px solid #2848BE",
				cursorborderradius:"0px"
				
			});
			
			//list home tour
			$(".listhometour, .listarchivedestination").niceScroll({
				touchbehavior:true,
				cursorcolor:"#EBEEF2",
				cursoropacitymax:0.9,
				cursorwidth:3,
				autohidemode:true,
				cursorborder:"0px solid #2848BE",
				cursorborderradius:"0px"
				
			});
		});
		//end scroll
		
		
		//start fancybox
		$(document).ready(function(){
						
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : true,
				arrows    : true,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
			
		});
		//end fancybox
		
		
		/* ]]> */
                
                $(document).ready(function() {
    $('.pgwSlider').pgwSlider();
});
	</script>
	<script src="<?php echo base_url(); ?>assets/forest/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/forest/js/jquery.validate.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value);
    }, "type the correct answer -_-");

// validate contact form
$(function() {
    $('#contact').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 7
            },
            message: {
                required: true,
                
            }
        },
        messages: {
            name: {
                required: "Please enter your name",
                minlength: "your name must consist of at least 2 characters"
            },
            email: {
                required: "Please enter valid email address"
            },
            phone: {
                required: "Please enter phone number",
                minlength: "your name phone number consist of at least 7 digits"
            },
            message: {
                required: "Please write something to send this form."
                
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                type:"POST",
                data: $(form).serialize(),
                url:"<?php echo base_url(); ?>cart/send_email",
                success: function() {
                    $('#contact :input').attr('disabled', 'disabled');
                    $('#contact').fadeTo( "slow", 0.1, function() {
                        $(this).find(':input').attr('disabled', 'disabled');
                        $(this).find('label').css('cursor','default');
                        $('#success').fadeIn();
                    });
                },
                error: function() {
                    $('#contact').fadeTo( "slow", 0.15, function() {
                        $('#error').fadeIn();
                    });
                }
            });
        }
    });
});
</script>
    
</body>  
</html>