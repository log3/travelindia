<!--start navigation-->
<ul class="sf-menu" id="nav">
    
    <li class="current orange">
    	<span class="menufilter"></span>
        <a href="<?php echo base_url(); ?>"><strong>HOME</strong></a>
    </li>
    <li class="orange">
    	<span class="menufilter"></span>
        <a href="<?php echo base_url(); ?>about-us"><strong>ABOUT US</strong></a>
    </li>
    
    <li class="orange">
    	<span class="menufilter"></span>
        <a href="#"><strong>TOURS</strong></a>
    <ul class="nav">
						<?php if(isset($this->categories[0])):?>
                                                <?php foreach($this->categories[0] as $cat_menu):?>
						<li class="dropdown"><a href="<?php echo site_url($cat_menu->slug);?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $cat_menu->name;?><b class="caret"></b></a>
<!--							<ul class="dropdown-menu">
                                                            <?php foreach($this->categories[0] as $cat_menu):?>
								<li <?php echo $cat_menu->active ? 'class="active"' : false; ?>><a href="<?php echo site_url($cat_menu->slug);?>"><?php echo $cat_menu->name;?></a></li>
                                                            <?php endforeach;?>
							</ul>-->
						</li>	
                                                <?php endforeach;?>
						<?php
						endif;
						
						if(isset($this->pages[0]))
						{
							foreach($this->pages[0] as $menu_page):?>
								<li>
								<?php if(empty($menu_page->content)):?>
									<a href="<?php echo $menu_page->url;?>" <?php if($menu_page->new_window ==1){echo 'target="_blank"';} ?>><?php echo $menu_page->menu_title;?></a>
								<?php else:?>
									<a href="<?php echo site_url($menu_page->slug);?>"><?php echo $menu_page->menu_title;?></a>
								<?php endif;?>
								</li>
								
							<?php endforeach;	
						}
						?>
					</ul>
        </li>
    
    
    
<!--    <li class="orange">
    	<span class="menufilter"></span>
        <a href="#"><strong>PAGES</strong></a>
        <ul>
            <li>
                <a href="prices.php">Our Prices</a>
            </li>
            <li>
                <a href="tours.php">Our Tours</a>
            </li>
            <li>
                <a href="destinations.php">Our Destinations</a>
            </li>
            <li>
                <a href="about-us.php">About us</a>
                
                <ul>
                    <li><a href="about-us.php">First Solution</a></li>
                    <li><a href="about-us-2.php">Second Solution</a></li>
					<li><a href="about-us-3.php">Third Solution</a></li>
                </ul>
                
            </li>
            <li>
                <a href="team.php">Our Team</a>
            </li>
            <li>
                <a href="single-project.php">Single Project</a>
            </li>
            <li>
                <a href="page-right-sidebar.php">Page</a>
                
                <ul>
                    <li><a href="page-right-sidebar.php">With right sidebar</a></li>
                    <li><a href="page-left-sidebar.php">With left sidebar</a></li>
                    <li><a href="page-full-width.php">Full Width</a></li>
                    <li><a href="page-full-width-centered.php">Centered Page</a></li>
                </ul>
                
            </li>
            <li>
                <a href="columns.php">Grid System</a>
            </li>
		</ul>
    </li>-->
    
<!--    <li class="red">
    	<span class="menufilter"></span>
        <a href="#"><strong>GALLERY</strong></a>
        <ul>
            <li>
                <a href="#">Gallery with same size</a>
                
                <ul>
                    <li><a href="gallery-2-column.php">2 column</a></li>
                    <li><a href="gallery-3-column.php">3 column</a></li>
                    <li><a href="gallery-4-column.php">4 column</a></li>
                </ul>
                
            </li>
            <li>
                <a href="#">Gallery With Filter</a>
                
                <ul>
                    <li><a href="gallery-with-filter-2-column.php">2 column</a></li>
                    <li><a href="gallery-with-filter-3-column.php">3 column</a></li>
                    <li><a href="gallery-with-filter-4-column.php">4 column</a></li>
                </ul>
                
            </li>
            <li>
                <a href="#">Gallery Masonry</a>
                
                <ul>
                    <li><a href="gallery-masonry-2-column.php">2 column</a></li>
                    <li><a href="gallery-masonry-3-column.php">3 column</a></li>
                    <li><a href="gallery-masonry-4-column.php">4 column</a></li>
                </ul>
                
            </li>
            <li>
                <a href="#">Gallery Masonry With Filter</a>
                
                <ul>
                    <li><a href="gallery-masonry-with-filter-2-column.php">2 column</a></li>
                    <li><a href="gallery-masonry-with-filter-3-column.php">3 column</a></li>
                    <li><a href="gallery-masonry-with-filter-4-column.php">4 column</a></li>
                </ul>
                
            </li>
		</ul>
    </li>-->
    
<!--    <li class="violet">
    	<span class="menufilter"></span>
        <a href="#"><strong>BLOG</strong></a>
        <ul>
            <li>
                <a href="blog-standard-right-sidebar.php">Blog Standard</a>
                
                <ul>
                    <li><a href="blog-standard-right-sidebar.php">With right sidebar</a></li>
                    <li><a href="blog-standard-left-sidebar.php">With left sidebar</a></li>
                    <li><a href="blog-standard-full-width.php">Full Width Archive</a></li>
                    <li><a href="blog-standard-full-width-centered.php">Centered Archive</a></li>
                </ul>
                
            </li>
            <li>
                <a href="blog-masonry-3-column.php">Blog Masonry Infinite Scroll</a>
                
                <ul>
                    <li><a href="blog-masonry-2-column.php">2 columns</a></li>
                    <li><a href="blog-masonry-3-column.php">3 columns</a></li>
                    <li><a href="blog-masonry-4-column.php">4 columns</a></li>
                </ul>
                
            </li>
            <li>
                <a href="blog-masonry-3-column-pagination.php">Blog Masonry With Pagination</a>
                
                <ul>
                    <li><a href="blog-masonry-2-column-pagination.php">2 columns</a></li>
                    <li><a href="blog-masonry-3-column-pagination.php">3 columns</a></li>
                    <li><a href="blog-masonry-4-column-pagination.php">4 columns</a></li>
                </ul>
                
            </li>
            <li>
                <a href="single-post-right-sidebar.php">Single Post</a>
                
                <ul>
                    <li><a href="single-post-right-sidebar.php">With right sidebar</a></li>
                    <li><a href="single-post-left-sidebar.php">With left sidebar</a></li>
                    <li><a href="single-post-full-width.php">Full Width Post</a></li>
                    <li><a href="single-post-full-width-centered.php">Centered Post</a></li>
                </ul>
                
            </li>
		</ul>
    </li>-->
    
    <li class="orange">
    	<span class="menufilter"></span>
        <a href="<?php echo base_url(); ?>contact-us"><strong>CONTACT US</strong></a>
<!--        <ul>
            <li>
                <a href="contact-dark.php">Dark Map</a>
            </li>
            <li>
                <a href="contact-light.php">Color Map</a>
            </li>
			<li>
                <a href="contact-with-text.php">With Text</a>
            </li>
		</ul>-->
    </li>
    
   	
</ul>   
<!--end navigationmenu-->