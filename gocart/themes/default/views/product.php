<?php $days=0; foreach($product->images as $image){
$days++; }
?>
<section class="header-page fade-up header-page-single-project">
	<div class="bounce-in animate4"><h2 class="header-pagetitle"><?php echo $page_title; ?></h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    
    
        <!--start flexslider gallery-->
        <div class="grid_8 green fade-left animate1">
        
            
             <!--start title and price-->
            <div class="titlesingleproject">
<!--            	<div class="pricesingleproject">
                    <p><?php echo $product->name;?></p>
                </div>-->
                <h4 style="font-size: 26px; ;color: #ffffff;padding: 20px;"><?php echo $product->name;?></h4>
            </div>
            <!--end title and price-->
            
            <!--start content flexslider-->
            <div class="contentflexslider">
            
            	<!--start flexslider-->
                <div class="flexslider flex-slider">
                    <ul class="slides">
                        
     
                <?php foreach($product->images as $image):?>
                        <li><img alt="" onclick="$(this).squard('390', $('#primary-img'));" src="<?php echo base_url('uploads/images/full/'.$image->filename);?>" /></li>
                <!--<img class="span1" onclick="$(this).squard('390', $('#primary-img'));" src="<?php echo base_url('uploads/images/medium/'.$image->filename);?>"/>-->
                <?php endforeach;?>
            
                        
                    </ul>
                </div>
                <!--end flexslider-->
                
                
                <!--start flexslider carousel-->
                <?php if(count($product->images) > 1):?>
                <div class="flexslider flex-carousel">
                    <ul class="slides">
                          <?php foreach($product->images as $image):?>
                        <li><img alt="" src="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" /></li>
                <!--<img class="span1" onclick="$(this).squard('390', $('#primary-img'));" src="<?php echo base_url('uploads/images/medium/'.$image->filename);?>"/>-->
                <?php endforeach;?>
                        
                    </ul>
                </div>
                 <?php endif;?>
                <!--end flexslider carousel-->
            
            </div>
            <!--end content flexslider-->
            
        
        </div>
        <!--end flexslider gallery-->
        
        
        <!--start info project-->
        <div class="grid_4 green fade-right animate1">
        
        	<!--title date and day-->
            <div class="datedaysingleproject">
                <p class="datesingleproject"><?php echo format_currency($product->saleprice); ?></p>
                <p class="datesingleproject"></p>
                <div class="daysingleproject">
                    <p><?php echo $days; ?></p>
                    <span>DAYS</span>
                </div>
            </div>
            <!--title date and day-->
            
            <!--start content info-->
            <div class="contentinfo">
            	               
                <blockquote>Duration: <?php echo $days; ?> days<br/>Price: <?php echo format_currency($product->saleprice); ?> per person</blockquote>
                <p><?php echo $product->description; ?></p><br/>
                <a class="btn" id="open-pop-up-3" href="#pop-up-3"><p>CONTACT US</p></a>
                <!--<a id="open-pop-up-3" href="#pop-up-3" class="btn">Custom buttons</a>-->
                <div id="pop-up-3" class="pop-up-display-content">
                    
<div id="success">
                        <span class="green textcenter" style="position:relative;width: none;">
                            <p style="margin-top: 1em;">Your message was sent successfully! We will be in touch as soon as We can.</p>
                            </span>
                        </div>

                        <div id="error">
                            <span>
                                <p>Something went wrong, try refreshing and submitting the form again.</p>
                            </span>
                        </div>

                        <form id="contact" name="contact" method="post" novalidate="novalidate">
                            <fieldset>
                        <!--        <label for="name" id="name">Name<span class="required">*</span>
                                </label>-->
                                <input type="text" name="name" id="name" size="30" value="" required="" placeholder="Name">
                        <!--        <label for="email" id="email">Email<span class="required">*</span>
                                </label>-->
                                <input type="text" name="email" id="email" size="30" value="" required="" placeholder="Email Address">
                                <!--<label for="phone" id="phone">Phone<span class="required">*</span></label>-->
                                <input type="text" name="phone" id="phone" size="30" value="" placeholder="Phone Number">
                        <!--        <label for="Message" id="message">Message<span class="required">*</span>
                                </label>-->
                                <textarea name="message" id="message" required="" placeholder="Message"></textarea>

                                <input id="submit" type="submit" name="submit" value="Send" style="margin-top:15px">
                            </fieldset>
                        </form>
                </div>
            </div>
            <!--end content info-->
        
        </div>
        <!--end info project-->
        
        <div class="divider"><span></span></div>
        
	</div>
    <!--end container-->
   
    <!--start container for arrows-->
    <div class="container arrowscarousel green clearfix">
        
        <!--start arrows carousel-->
        <div class="grid_6">
            <div id="showbiz_left_2" class="arrowcarouselprev fade-right"></div>
        </div>
        <div class="grid_6">
            <div id="showbiz_right_2" class="arrowcarouselnext fade-left"></div>
        </div>
        <!--end arrows carousel-->
        
    </div>
    <!--end container for arrows--> 
       
    <!--start carousel-->
    <div class="container clearfix showbiz-container">
    
    
        <div class="showbiz" data-left="#showbiz_left_2" data-right="#showbiz_right_2" data-play="#showbiz_play_2">
            <div class="overflowholder">
                <ul> 
                <?php 
                $i=0;
                foreach($product->images as $image):
                    $i++; 
                    ?>
                        <!--<li><img alt="" src="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" /></li>-->
                <!--<img class="span1" onclick="$(this).squard('390', $('#primary-img'));" src="<?php echo base_url('uploads/images/medium/'.$image->filename);?>"/>-->
                
                
                    <li>
        
                        <!--start first destination-->
                        <div class="destinationsingleproject single-carousel blue">
                            
                            <img alt="" class="imgdestinationsingleproject" src="<?php echo base_url('uploads/images/medium/'.$image->filename);?>">
                            
                            <div class="titledaydestinationsingleproject">
                                <p class="titledestinationsingleproject"><?php echo $image->alt; ?></p>
                                <div class="daydestinationsingleproject">
                                    <p><?php echo $i; ?></p>
                                    <span>DAY</span>
                                </div>
                            </div> 
                            
                            <p class="descriptiondestinationsingleproject"><?php echo $image->caption; ?>.</p>
                            
                            <a class="readmoredestinationsingleproject rotate" href="#"></a>  
                               
                        </div>
                        <!--end first destination-->
                        
                    </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
        

    </div>
    <!--end carousel-->
            
            
</section>
<!--end internal page-->

<div class="divider"><span></span></div>



	<!--Start js-->    
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.min.js"></script> <!--Jquery-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="<?php echo base_url(); ?>assets/forest/js/excanvas.js"></script> <!--canvas need for ie-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/forest/showbizpro/js/jquery.themepunch.plugins.min.js"></script> <!--showbiz-->						
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/forest/showbizpro/js/jquery.themepunch.showbizpro.min.js"></script> <!--showbiz-->
    <script src="<?php echo base_url(); ?>assets/forest/js/scroolto.js"></script> <!--Scrool To-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.inview.min.js"></script> <!--inview-->
	<script src="<?php echo base_url(); ?>assets/forest/js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="<?php echo base_url(); ?>assets/forest/js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="<?php echo base_url(); ?>assets/forest/js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
    <script src="<?php echo base_url(); ?>assets/forest/js/jquery.flexslider-min.js"></script> <!--flexslider-->
	<script src="<?php echo base_url(); ?>assets/forest/js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="<?php echo base_url(); ?>assets/forest/js/settings.js"></script> <!--settings-->
    <script src="<?php echo base_url(); ?>assets/forest/js/popupwindow.js"></script>
<script src="<?php echo base_url(); ?>assets/forest/js/demo.js"></script>
    <!--End js-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/forest/css/popupwindow.css">
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//start flexslider
		jQuery(document).ready(function() {
		  
		  $('.flex-carousel').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 100,
			itemMargin: 5,
			asNavFor: '.flex-slider'
		  });

		  $('.flex-slider').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			sync: ".flex-carousel",
			start: function(slider){
                            $('body').removeClass('loading');
			}
		  });
		  
		});
		//end flexslider
		
		
		//start carousel
		jQuery(document).ready(function() {

			jQuery('.showbiz-container').showbizpro({
				dragAndScroll:"on",
				visibleElementsArray:[4,3,2,1]
			});
		   
		});
		//end carousel
		

		/* ]]> */
	</script>
        <!-- Vendor JS -->
<!--<script src="//code.jquery.com/jquery-latest.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/forest/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/forest/js/jquery.validate.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value);
    }, "type the correct answer -_-");

// validate contact form
$(function() {
    $('#contact').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 7
            },
            message: {
                required: true,
                
            }
        },
        messages: {
            name: {
                required: "come on, you have a name don't you?",
                minlength: "your name must consist of at least 2 characters"
            },
            email: {
                required: "no email, no message"
            },
            phone: {
                required: "Please enter phone number",
                minlength: "your name phone number consist of at least 7 digits"
            },
            message: {
                required: "um...yea, you have to write something to send this form.",
                minlength: "thats all? really?"
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                type:"POST",
                data: $(form).serialize(),
                url:"<?php echo base_url(); ?>cart/send_email",
                success: function() {
                    $('#contact :input').attr('disabled', 'disabled');
                    $('#contact').fadeTo( "slow", 0.1, function() {
                        $(this).find(':input').attr('disabled', 'disabled');
                        $(this).find('label').css('cursor','default');
                        $('#success').fadeIn();
                    });
                },
                error: function() {
                    $('#contact').fadeTo( "slow", 0.15, function() {
                        $('#error').fadeIn();
                    });
                }
            });
        }
    });
});
</script>
        
    
</body>  
</html>