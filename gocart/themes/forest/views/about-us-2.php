<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-about-us">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">ABOUT US<br/><span class="header-pagedescription">Second Solution</span></h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
        
        
        <div class="grid_6 blue fade-left animate1">
        	<h2 class="titlewithborder"><span>OUR AGENCY</span></h2>
			<div class="dividerheight20"></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl</p><br/>
            
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl</p>
        </div>
        
        <div class="grid_6 green fade-right animate1">
        	<h2 class="titlewithborder"><span>OUR SKILLS</span></h2>
			<div class="dividerheight10"></div>
			<div class="progressbar green fade-right animate1">
             	<h4 class="progressbartitle" style="width:100%"><span>HONEYMOON - 100%</span></h4>   
            </div>
            <div class="progressbar red fade-right animate2">
             	<h4 class="progressbartitle" style="width:70%"><span>PACKAGE TOURS - 70%</span></h4>   
            </div>  
            <div class="progressbar orange fade-right animate3">
             	<h4 class="progressbartitle" style="width:60%"><span>FLY AND DRIVE - 60%</span></h4>   
            </div>  
            <div class="progressbar violet fade-right animate4">
             	<h4 class="progressbartitle" style="width:90%"><span>TRAVEL RELAX - 90%</span></h4>   
            </div> 
        </div>
        
        <div class="grid_12"></div>
        
        <div class="grid_6 blue fade-left animate1">
        	<h2 class="titlewithborder"><span>OUR TESTIMONIALS</span></h2>
			<div class="dividerheight20"></div>
            <div class="textevidence">
            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. <i>- John Doe -</i></p>
            </div>
            <br/>
            <div class="textevidence">
            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. <i>- Jane Hope -</i></p>
            </div>
            <br/>
            <div class="textevidence">
            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. <i>- Nick Spitch -</i></p>
            </div>
             
        </div>
        
        
        <div class="grid_6 blue fade-right animate1">
            <h2 class="titlewithborder"><span>OUR FEATURES</span></h2>
            
			<div class="dividerheight20"></div>
            
            <!--start accordion-->
            <div class="accordion accordionlight">
                
                <h4 class="blue-borderleft firstaccordiontitle">CREATIVE</h4>
                <div>
                    <p>
                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>
                </div>
                
                <h4 class="green-borderleft">DYNAMIC</h4>
                <div>
                    <p>
                     Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>
                </div>
                
                <h4 class="orange-borderleft">PROFESSIONAL</h4>
                <div>
                    <p>
                     Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>

                </div>
                
            </div>
            <!--end accordion-->
            	
        </div>
        


        
        <div class="grid_12 orange">
                <br/><h2 class="titlewithborder"><span>MEET OR TEAM</span></h2>
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member orange fade-left animate1">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member1.jpg">
                </div>
                <h4 class="membername">JANE MC DOE</h4>
                <p class="memberposition"><i>Owner</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member blue fade-left animate2">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member2.jpg">
                </div>
                <h4 class="membername">NICK HOPE</h4>
                <p class="memberposition"><i>Cofounder</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member green fade-left animate3">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member3.jpg">
                </div>
                <h4 class="membername">JULIETTE LIGHT</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member red fade-left animate4">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member4.jpg">
                </div>
                <h4 class="membername">MARK SPITCH</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
       
        
        <div class="grid_12 blue">
                <br/><h2 class="titlewithborder"><span>OUR CLIENTS</span></h2>
        </div>
       
        <div class="grid_3">
        	<div class="imgclient fade-left animate1">
                <img alt="" src="img/clients/client1.png">    
            </div>
        </div>
        
       	<div class="grid_3">
            <div class="imgclient fade-left animate2">
                <img alt="" src="img/clients/client2.png">     
            </div>
        </div>
        
        <div class="grid_3">
            <div class="imgclient fade-left animate3">
                <img alt="" src="img/clients/client3.png">    
            </div>
        </div>
        
        <div class="grid_3">
            <div class="imgclient fade-left animate4">
                <img alt="" src="img/clients/client4.png">        
            </div>
        </div>
        
        
        
	</div>
    <!--end container-->
   
  
            
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//start accordion
		$(document).ready(function() {
			$( ".accordion" ).accordion();
		});
		//end accordion
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		

		/* ]]> */
	</script>
    
</body>  
</html>	