<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-gallery">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">BLOG MASONRY PAGINATION<br/><span class="header-pagedescription">with 4 columns</span></h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    

		<!--start blogmasonry-->
		<div class="stylemasonry">

        
            <!--start post masonry-->
            <div class="grid_3 singlemasonry singlepostmasonry red">
                
                <img alt="" class="imgsinglepostmasonry" src="img/header-page/price.jpg">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_3 singlemasonry singlepostmasonry yellow">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_3 singlemasonry singlepostmasonry green">
                
                <!--soundcloud-->
                <iframe class="soundcloudiframe" height="166" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/110614747"></iframe>
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_3 singlemasonry singlepostmasonry blue">
                
                <img alt="" class="imgsinglepostmasonry" src="img/header-page/price.jpg">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_3 singlemasonry singlepostmasonry violet">
                
                <img alt="" class="imgsinglepostmasonry" src="img/header-page/price.jpg">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_3 singlemasonry singlepostmasonry orange">
                
                <!--vimeo-->
                <iframe src="//player.vimeo.com/video/79623167?title=0&amp;byline=0&amp;portrait=0&amp;color=ff9933" width="500" height="281" class="vimeoiframe"></iframe>
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            
            <!--start post masonry-->
            <div class="grid_3 singlemasonry singlepostmasonry red">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            
        </div>
        <!--end blogmasonry-->
        

        <!--start archive pagination-->
        <div class="contentarchivepagination paginationmasonry">
            <div class="archivepagination">
                <a title="Previous Posts" class="btn prevbtn tooltip" href="#"><img alt="" src="img/blog/prevpagination.png"></a>
                <a title="Next Posts" class="btn nextbtn tooltip" href="blog-masonry-with-4-column-2.php"><img alt="" src="img/blog/nextpagination.png"></a>
            </div>
        </div>
        <!--end archive pagination-->
        
        
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
    <script src="js/isotope/jquery.isotope.min.js"></script> <!--isotope-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		//start masonry
		jQuery(document).ready(function() {

			$(function(){
						  
			  //initialize
			  var $container = $('.stylemasonry');
			  

			  $container.isotope({
				itemSelector : '.singlemasonry'
			  });
			  //end initialize
			  
			  
			  //start masonry
			  $container.isotope({
				itemSelector : '.singlemasonry'
			  });
			  // end masonry
			  

			 //relayout 
			setInterval(function(){
				$container.isotope('reLayout');
			}, 0);
			//reLayout
			

			});		   

		});
		//end masonry
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip


		/* ]]> */
	</script>
    
</body>  
</html>