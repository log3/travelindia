<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-gallery">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">ARCHIVE BLOG<br/><span class="header-pagedescription">with left sidebar</span></h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    
    
        <!--start sidebar-->
        <div class="grid_4">
        
        	<?php include "include/sidebar.php"; ?>
        
        </div>
        <!--end sidebar-->
        
        
        <!--start content-->
        <div class="grid_8">
        
            <!--start post archive-->
            <div class="archivesinglepost blue">
                
                <!--infoarchivesinglepost-->
                <div class="infoarchivesinglepost">
                
                	<div class="datearchivesinglepost">
                    	<p>21</p>
                        <span>JAN</span>
                    </div>
                   
                    <div class="iconarchivesinglepost">
                        <a title="John Doe" class="tooltip" href=""><img alt="" src="img/blog/author.png"></a>
                        <a title="Tour America" class="tooltip" href=""><img alt="" src="img/blog/category.png"></a>
                        <a title="4 Comments" class="tooltip" href=""><img alt="" src="img/blog/comment.png"></a>
                	</div>
                    
                </div>
                <!--end infoarchivesinglepost-->
                
                <img alt="" class="imgarchivesinglepost" src="img/blog/imgtest1.jpg">
                
                <div class="conttitlearchivesinglepost">
                    <p class="titlearchivesinglepost">POST WITH IMAGE</p>
                </div> 
                
                <p class="descriptionarchivesinglepost">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="btn" href="#"><p>READ MORE</p></a> 
            
            </div>
            <!--end post archive-->
            
            
            <!--start post archive-->
            <div class="archivesinglepost violet">
                
                <!--infoarchivesinglepost-->
                <div class="infoarchivesinglepost">
                
                	<div class="datearchivesinglepost">
                    	<p>21</p>
                        <span>JAN</span>
                    </div>
                   
                    <div class="iconarchivesinglepost">
                        <a title="John Doe" class="tooltip" href=""><img alt="" src="img/blog/author.png"></a>
                        <a title="Tour America" class="tooltip" href=""><img alt="" src="img/blog/category.png"></a>
                        <a title="4 Comments" class="tooltip" href=""><img alt="" src="img/blog/comment.png"></a>
                	</div>
                    
                </div>
                <!--end infoarchivesinglepost-->
                
                <!--vimeo-->
                <iframe src="//player.vimeo.com/video/79623167?title=0&amp;byline=0&amp;portrait=0&amp;color=C377E4" height="439" class="vimeoiframe"></iframe>
                
                <div class="conttitlearchivesinglepost">
                    <p class="titlearchivesinglepost">POST VIMEO VIDEO</p>
                </div> 
                
                <p class="descriptionarchivesinglepost">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="btn" href="#"><p>READ MORE</p></a> 
            
            </div>
            <!--end post archive-->
            
            
            <!--start post archive-->
            <div class="archivesinglepost red">
                
                <!--infoarchivesinglepost-->
                <div class="infoarchivesinglepost">
                
                	<div class="datearchivesinglepost">
                    	<p>21</p>
                        <span>JAN</span>
                    </div>
                   
                    <div class="iconarchivesinglepost">
                        <a title="John Doe" class="tooltip" href=""><img alt="" src="img/blog/author.png"></a>
                        <a title="Tour America" class="tooltip" href=""><img alt="" src="img/blog/category.png"></a>
                        <a title="4 Comments" class="tooltip" href=""><img alt="" src="img/blog/comment.png"></a>
                	</div>
                    
                </div>
                <!--end infoarchivesinglepost-->
               
                
                <div class="conttitlearchivesinglepost">
                    <p class="titlearchivesinglepost">NORMAL POST</p>
                </div> 
                
                <p class="descriptionarchivesinglepost">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="btn" href="#"><p>READ MORE</p></a> 
            
            </div>
            <!--end post archive-->
            
            
            
            <!--start post archive-->
            <div class="archivesinglepost green">
                
                <!--infoarchivesinglepost-->
                <div class="infoarchivesinglepost">
                
                	<div class="datearchivesinglepost">
                    	<p>21</p>
                        <span>JAN</span>
                    </div>
                   
                    <div class="iconarchivesinglepost">
                        <a title="John Doe" class="tooltip" href=""><img alt="" src="img/blog/author.png"></a>
                        <a title="Tour America" class="tooltip" href=""><img alt="" src="img/blog/category.png"></a>
                        <a title="4 Comments" class="tooltip" href=""><img alt="" src="img/blog/comment.png"></a>
                	</div>
                    
                </div>
                <!--end infoarchivesinglepost-->
                
                <!--youtube-->
                <iframe height="470" src="//www.youtube.com/embed/A3PDXmYoF5U" class="vimeoiframe" allowfullscreen></iframe>
                
                <div class="conttitlearchivesinglepost">
                    <p class="titlearchivesinglepost">POST YOUTUBE VIDEO</p>
                </div> 
                
                <p class="descriptionarchivesinglepost">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="btn" href="#"><p>READ MORE</p></a> 
            
            </div>
            <!--end post archive-->
            
            
            
            <!--start post archive-->
            <div class="archivesinglepost orange">
                
                <!--infoarchivesinglepost-->
                <div class="infoarchivesinglepost">
                
                	<div class="datearchivesinglepost">
                    	<p>21</p>
                        <span>JAN</span>
                    </div>
                   
                    <div class="iconarchivesinglepost">
                        <a title="John Doe" class="tooltip" href=""><img alt="" src="img/blog/author.png"></a>
                        <a title="Tour America" class="tooltip" href=""><img alt="" src="img/blog/category.png"></a>
                        <a title="4 Comments" class="tooltip" href=""><img alt="" src="img/blog/comment.png"></a>
                	</div>
                    
                </div>
                <!--end infoarchivesinglepost-->
                
                <!--soundcloud-->
                <iframe class="soundcloudiframe" height="166" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/110614747"></iframe>
                
                <div class="conttitlearchivesinglepost">
                    <p class="titlearchivesinglepost">POST SOUNCLOUD MUSIC</p>
                </div> 
                
                <p class="descriptionarchivesinglepost">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="btn" href="#"><p>READ MORE</p></a> 
            
            </div>
            <!--end post archive-->
            
        	<!--start archive pagination-->
            <div class="contentarchivepagination">
                <div class="archivepagination">
                    <a title="Previous Posts" class="btn prevbtn tooltip" href="#"><img alt="" src="img/blog/prevpagination.png"></a>
                    <a title="Next Posts" class="btn nextbtn tooltip" href="#"><img alt="" src="img/blog/nextpagination.png"></a>
                </div>
            </div>
            <!--end archive pagination-->
        
        </div>
        <!--end content-->
            
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		

		/* ]]> */
	</script>
    
</body>  
</html>