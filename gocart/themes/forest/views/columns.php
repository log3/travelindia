<?php include "include/header.php"; ?>

<section class="header-page fade-up">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">COLUMNS<br/><span class="header-pagedescription">12 columns grid system</span></h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
        
        
        <div class="grid_1 blue">
        	<div class="textevidence">grid_1</div>
        </div>
        <div class="grid_11 blue">
        	<div class="textevidence">grid_11</div>
        </div>
        
         
        <div class="grid_2 green">
        	<div class="textevidence">grid_2</div>
        </div>
        <div class="grid_10 green">
        	<div class="textevidence">grid_10</div>
        </div>
        
        
        <div class="grid_3 yellow">
        	<div class="textevidence">grid_3</div>
        </div> 
        <div class="grid_9 yellow">
        	<div class="textevidence">grid_9</div>
        </div>  
        
        
        <div class="grid_4 orange">
        	<div class="textevidence">grid_4</div>
        </div> 
        <div class="grid_8 orange">
        	<div class="textevidence">grid_8</div>
        </div> 
        
        
        <div class="grid_5 red">
        	<div class="textevidence">grid_5</div>
        </div>
        <div class="grid_7 red">
        	<div class="textevidence">grid_7</div>
        </div>
        
        
        <div class="grid_6 violet">
        	<div class="textevidence">grid_6</div>
        </div> 
        <div class="grid_6 violet">
        	<div class="textevidence">grid_6</div>
        </div> 
        
        
        <div class="grid_7 blue">
        	<div class="textevidence">grid_7</div>
        </div>
        <div class="grid_5 blue">
        	<div class="textevidence">grid_5</div>
        </div>
        
        
        <div class="grid_8 green">
        	<div class="textevidence">grid_8</div>
        </div>
        <div class="grid_4 green">
        	<div class="textevidence">grid_4</div>
        </div>
        
        
        <div class="grid_9 yellow">
        	<div class="textevidence">grid_9</div>
        </div>
        <div class="grid_3 yellow">
        	<div class="textevidence">grid_3</div>
        </div>
        
        
        <div class="grid_10 orange">
        	<div class="textevidence">grid_10</div>
        </div>
        <div class="grid_2 orange">
        	<div class="textevidence">grid_2</div>
        </div>
        
        
        <div class="grid_11 red">
        	<div class="textevidence">grid_11</div>
        </div> 
        <div class="grid_1 red">
        	<div class="textevidence">grid_1</div>
        </div> 
        
        
        <div class="grid_12 violet">
        	<div class="textevidence">grid_12</div>
        </div>

        
	</div>
    <!--end container-->
   
  
            
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		

		/* ]]> */
	</script>
    
</body>  
</html>