<?php include "include/header.php"; ?>

<!--start contact-->
<div class="contactmap">
	
    <!--start info contact-->
    <div class="infocontact infocontactdark blue">
    
    	<div class="contentinfocontact">
        	<p class="titleinfocontact">LOVE TRAVEL</p>
            <ul>
            	<li><p><img alt="" src="img/contact/iconinfocontactaddress.png">102 Madison Ave - New York ( NY )</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactphone.png">Telephone: (0039) 34916656398</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactmail.png">Mail: yourname@yourdomain.xx</p></li>
            </ul>
        </div>
        
        <div class="triangleinfocontact">
        	<span></span>
        </div>
        
    	<!--start marker-->
        <div class="markercontactmap">
            <div class="circlemarker"><div class="innercirclemarker"></div></div>
            <div class="trianglemarker"></div>
        </div>
        <!--end marker-->
    
    </div>
    <!--end info contact-->
    
	<!--google maps-->
    <div id="map-canvas" class="map-canvas-dark"></div>
    <!--google maps-->
    	
</div>
<!--end contactmap-->

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    	
        <!--start form-->
        <form class="contactform">
        
            <div class="grid_4 red">
                <ul>
                    <li class="filterinputicon"><div class="inputicon inputfirstname"></div></li>
                    <li><input value="Name" type="text"></li>
                </ul>
            </div>
            
            <div class="grid_4 green">
                <ul>
                    <li class="filterinputicon"><div class="inputicon inputlastname"></div></li>
                    <li><input value="Surname" type="text"></li>
                </ul>
            </div>
            
            <div class="grid_4 orange">
                <ul>
                    <li class="filterinputicon"><div class="inputicon inputemail"></div></li>
                    <li><input value="Email" type="text"></li>
                </ul>
            </div>
            
            <div class="grid_4 violet">
                <ul>
                    <li class="filterinputicon"><div class="inputicon inputdate"></div></li>
                    <li><input value="Date" type="text"></li>
                </ul>
            </div>
            
            <div class="grid_4 blue">
                <ul>
                    <li class="filterinputicon"><div class="inputicon inputphone"></div></li>
                    <li><input value="Phone" type="text"></li>
                </ul>
            </div>
            
            <div class="grid_4 yellow">
                <ul>
                    <li class="filterinputicon"><div class="inputicon inputobject"></div></li>
                    <li><input value="Object" type="text"></li>
                </ul>
            </div>
            
            <div class="grid_12">
                <textarea>Message</textarea>
            </div>
            
            <div class="grid_12">
                <input value="SEND" type="submit">
            </div>
        
        </form>
        <!--end form-->
    
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script> <!-- Google Map API -->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		//start map
		$(document).ready(function(){
		
			var map;
			var brooklyn = new google.maps.LatLng(40.759277, -73.977064);
			
			var MY_MAPTYPE_ID = 'custom_style';
			
			function initialize() {
				
				
			  var mapcanvasdark = $('.map-canvas-dark').length;
			
			  
			  if (mapcanvasdark==1){
				  
				var featureOpts = [
			 
				  {
					"stylers": [
					  { "saturation": -100 }
					]
				  },{
				  }
			  ];	
				  
			  }else{
				  
				var featureOpts = [];	  
				  
			  }
			  
			  var mapOptions = {
				zoom: 14,
				draggable: false,
				center: brooklyn,
				disableDefaultUI: true,
				scrollwheel: false,
				mapTypeControlOptions: {
				  mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
				},
				mapTypeId: MY_MAPTYPE_ID
			  };
			
			  map = new google.maps.Map(document.getElementById('map-canvas'),
				  mapOptions);
			
			  var styledMapOptions = {
				name: 'Custom Style'
			  };
			
			  var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
			
			  map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
			}
			
			google.maps.event.addDomListener(window, 'load', initialize);
		
		});
		//end map
		

		/* ]]> */
	</script>
    
</body>  
</html>