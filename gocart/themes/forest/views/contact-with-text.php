<?php include "include/header.php"; ?>

<!--start contact-->
<div class="contactmap">
	
    <!--start info contact-->
    <div class="infocontact infocontactlight blue">
    
    	<div class="contentinfocontact">
        	<p class="titleinfocontact">LOVE TRAVEL</p>
            <ul>
            	<li><p><img alt="" src="img/contact/iconinfocontactaddress.png">102 Madison Ave - New York ( NY )</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactphone.png">Telephone: (0039) 34916656398</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactmail.png">Mail: yourname@yourdomain.xx</p></li>
            </ul>
        </div>
        
        <div class="triangleinfocontact">
        	<span></span>
        </div>
        
    	<!--start marker-->
        <div class="markercontactmap">
            <div class="circlemarker"><div class="innercirclemarker"></div></div>
            <div class="trianglemarker"></div>
        </div>
        <!--end marker-->
    
    </div>
    <!--end info contact-->
    
	<!--google maps-->
    <div id="map-canvas"></div>
    <!--google maps-->
    	
</div>
<!--end contactmap-->

<div class="divider"><span></span></div>

<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    	
		<!--accordion-->
		<div class="grid_4 violet fade-left">
			<h2 class="titlewithborder"><span>OUR FEATURES</span></h2>
            
			<div class="dividerheight20"></div>
            
            <!--start accordion-->
            <div class="accordion accordionlight">
                
                <h4 class="violet-borderleft firstaccordiontitle">CREATIVE</h4>
                <div>
                    <p>
                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>
                </div>
                
                <h4 class="violet-borderleft">DYNAMIC</h4>
                <div>
                    <p>
                     Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>
                </div>
                
                <h4 class="violet-borderleft">PROFESSIONAL</h4>
                <div>
                    <p>
                     Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                    </p>

                </div>
                
            </div>
            <!--end accordion-->
		</div>
		<!--end accordion-->
		
		<!--simple text-->
		<div class="grid_4 blue fade-up">
			<h2 class="titlewithborder"><span>OUR AGENCY</span></h2>
			<div class="dividerheight20"></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><br/>
			<blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc.</blockquote>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit.</p>
            
		</div>
		<!--end simple text-->
		
		<!--start form-->
		<div class="grid_4 green fade-right">
			<h2 class="titlewithborder"><span>CONTACT US</span></h2>
			
			<!--start form-->
			<form class="contactform">
				
				<div class="dividerheight20"></div>	
				
				<ul>
                    <li class="filterinputicon"><div class="inputicon inputfirstname"></div></li>
                    <li><input value="Name" type="text"></li>
                </ul>
				
				<div class="dividerheight20"></div>
				
				<ul>
                    <li class="filterinputicon"><div class="inputicon inputemail"></div></li>
                    <li><input value="Email" type="text"></li>
                </ul>
				
				<div class="dividerheight20"></div>
				
				<textarea>Message</textarea>
				
				<div class="dividerheight20"></div>
				
				<input value="SEND" type="submit">
			
			</form>
			<!--end form-->
			
		</div>
		<!--end form-->
		
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script> <!-- Google Map API -->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		//start map
		$(document).ready(function(){
		
			var map;
			var brooklyn = new google.maps.LatLng(40.759277, -73.977064);
			
			var MY_MAPTYPE_ID = 'custom_style';
			
			function initialize() {
				
				
			  var mapcanvasdark = $('.map-canvas-dark').length;
			
			  
			  if (mapcanvasdark==1){
				  
				var featureOpts = [
			 
				  {
					"stylers": [
					  { "saturation": -100 }
					]
				  },{
				  }
			  ];	
				  
			  }else{
				  
				var featureOpts = [];	  
				  
			  }
			  
			  var mapOptions = {
				zoom: 14,
				draggable: false,
				center: brooklyn,
				disableDefaultUI: true,
				scrollwheel: false,
				mapTypeControlOptions: {
				  mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
				},
				mapTypeId: MY_MAPTYPE_ID
			  };
			
			  map = new google.maps.Map(document.getElementById('map-canvas'),
				  mapOptions);
			
			  var styledMapOptions = {
				name: 'Custom Style'
			  };
			
			  var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
			
			  map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
			}
			
			google.maps.event.addDomListener(window, 'load', initialize);
		
		});
		//end map
		
		//start accordion
		$(document).ready(function() {
			$( ".accordion" ).accordion();
		});
		//end accordion
		

		/* ]]> */
	</script>
    
</body>  
</html>