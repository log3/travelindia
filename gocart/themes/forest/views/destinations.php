<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-destinations">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">THE BEST DESTINATIONS</h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    
		
        <div class="grid_4">
        	<!--start archive destination-->
            <div class="archivedestination archivedestination-0 orange fade-up animate1">
                
                <!--left-->
                <div class="leftarchivedestination">
                
                    <a href="gallery-masonry-with-filter-3-column.php">
                        <div class="btngalleryarchivedestination rotate"></div>
                    </a>
                    
                    <img alt="" src="img/destinations/photos/img1.jpg">
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivedestination">
                
                    <div class="titlearchivedestination">
                        <h4>AFRICA<span class="bulletarchivedestination"></span></h4>
                    </div>	
                    
                    <div class="listarchivedestination">
                    
                        <ul>
                            <li><a href="#"><p>Egypt</p></a></li>
                            <li><a href="#"><p>Kenya</p></a></li>
                            <li><a href="#"><p>Madagascar</p></a></li>
                            <li><a href="#"><p>Marocco</p></a></li>
                            <li><a href="#"><p>Mauritius</p></a></li>
                            <li><a href="#"><p>Lorem Ipsum</p></a></li>
                            <li><a href="#"><p>Dolor Sit</p></a></li>
                        </ul>
                    
                    </div>
                    
                    <!--start footer archivedestinations-->
                    <div class="footerarchivedestination">
                        
                        <div class="galleryarchivedestination">
                            <span>
                                <a class="tooltip" title="Gallery" href="gallery-masonry-with-filter-3-column.php"><img alt="" src="img/destinations/icongalleryarchivedestination.png"></a>
                            </span>
                        </div>
                       
                        <div class="promotionarchivedestination">
                            <span>
                                <a class="tooltip" title="Promotion" href="prices.php"><img alt="" src="img/destinations/iconpromotionarchivedestination.png"></a>
                            </span>
                        </div>
        
                    </div>
                    <!--end footer archivedestinations-->
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archive destination-->
        </div>
        
        <div class="grid_4">
        	<!--start archive destination-->
            <div class="archivedestination archivedestination-1 blue fade-up animate1">
                
                <!--left-->
                <div class="leftarchivedestination">
                
                    <a href="gallery-masonry-with-filter-3-column.php">
                        <div class="btngalleryarchivedestination rotate"></div>
                    </a>
                    
                    <img alt="" src="img/destinations/photos/img2.jpg">
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivedestination">
                
                    <div class="titlearchivedestination">
                        <h4>ASIA<span class="bulletarchivedestination"></span></h4>
                    </div>	
                    
                    <div class="listarchivedestination">
                    
                        <ul>
                            <li><a href="#"><p>Cambodia</p></a></li>
                            <li><a href="#"><p>China</p></a></li>
                            <li><a href="#"><p>Arab Emirates</p></a></li>
                            <li><a href="#"><p>Japan</p></a></li>
                            <li><a href="#"><p>Maldives</p></a></li>
                            <li><a href="#"><p>Lorem Ipsum</p></a></li>
                            <li><a href="#"><p>Dolor Sit</p></a></li>
                        </ul>
                    
                    </div>
                    
                    <!--start footer archivedestinations-->
                    <div class="footerarchivedestination">
                        
                        <div class="galleryarchivedestination">
                            <span>
                                <a class="tooltip" title="Gallery" href="gallery-masonry-with-filter-3-column.php"><img alt="" src="img/destinations/icongalleryarchivedestination.png"></a>
                            </span>
                        </div>
                       
                        <div class="promotionarchivedestination">
                            <span>
                                <a class="tooltip" title="Promotion" href="prices.php"><img alt="" src="img/destinations/iconpromotionarchivedestination.png"></a>
                            </span>
                        </div>
        
                    </div>
                    <!--end footer archivedestinations-->
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archive destination-->
        </div>
        
        <div class="grid_4">
        	<!--start archive destination-->
            <div class="archivedestination archivedestination-2 violet fade-up animate1">
                
                <!--left-->
                <div class="leftarchivedestination">
                
                    <a href="gallery-masonry-with-filter-3-column.php">
                        <div class="btngalleryarchivedestination rotate"></div>
                    </a>
                    
                    <img alt="" src="img/destinations/photos/img3.jpg">
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivedestination">
                
                    <div class="titlearchivedestination">
                        <h4>EUROPE<span class="bulletarchivedestination"></span></h4>
                    </div>	
                    
                    <div class="listarchivedestination">
                    
                        <ul>
                            <li><a href="#"><p>Portugal</p></a></li>
                            <li><a href="#"><p>Italy</p></a></li>
                            <li><a href="#"><p>Germany</p></a></li>
                            <li><a href="#"><p>Belgium</p></a></li>
                            <li><a href="#"><p>France</p></a></li>
                            <li><a href="#"><p>Lorem Ipsum</p></a></li>
                            <li><a href="#"><p>Dolor Sit</p></a></li>
                        </ul>
                    
                    </div>
                    
                    <!--start footer archivedestinations-->
                    <div class="footerarchivedestination">
                        
                        <div class="galleryarchivedestination">
                            <span>
                                <a class="tooltip" title="Gallery" href="gallery-masonry-with-filter-3-column.php"><img alt="" src="img/destinations/icongalleryarchivedestination.png"></a>
                            </span>
                        </div>
                       
                        <div class="promotionarchivedestination">
                            <span>
                                <a class="tooltip" title="Promotion" href="prices.php"><img alt="" src="img/destinations/iconpromotionarchivedestination.png"></a>
                            </span>
                        </div>
        
                    </div>
                    <!--end footer archivedestinations-->
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archive destination-->
        </div>
        
        <div class="grid_4">
        	<!--start archive destination-->
            <div class="archivedestination archivedestination-0 green fade-up animate1">
                
                <!--left-->
                <div class="leftarchivedestination">
                
                    <a href="gallery-masonry-with-filter-3-column.php">
                        <div class="btngalleryarchivedestination rotate"></div>
                    </a>
                    
                    <img alt="" src="img/destinations/photos/img4.jpg">
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivedestination">
                
                    <div class="titlearchivedestination">
                        <h4>N. AMERICA<span class="bulletarchivedestination"></span></h4>
                    </div>	
                    
                    <div class="listarchivedestination">
                    
                        <ul>
                            <li><a href="#"><p>California</p></a></li>
                            <li><a href="#"><p>New York</p></a></li>
                            <li><a href="#"><p>Florida</p></a></li>
                            <li><a href="#"><p>Arizona</p></a></li>
                            <li><a href="#"><p>Nevada</p></a></li>
                            <li><a href="#"><p>Lorem Ipsum</p></a></li>
                            <li><a href="#"><p>Dolor Sit</p></a></li>
                        </ul>
                    
                    </div>
                    
                    <!--start footer archivedestinations-->
                    <div class="footerarchivedestination">
                        
                        <div class="galleryarchivedestination">
                            <span>
                                <a class="tooltip" title="Gallery" href="gallery-masonry-with-filter-3-column.php"><img alt="" src="img/destinations/icongalleryarchivedestination.png"></a>
                            </span>
                        </div>
                       
                        <div class="promotionarchivedestination">
                            <span>
                                <a class="tooltip" title="Promotion" href="prices.php"><img alt="" src="img/destinations/iconpromotionarchivedestination.png"></a>
                            </span>
                        </div>
        
                    </div>
                    <!--end footer archivedestinations-->
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archive destination-->
        </div>
        
        <div class="grid_4">
        	<!--start archive destination-->
            <div class="archivedestination archivedestination-1 red fade-up animate1">
                
                <!--left-->
                <div class="leftarchivedestination">
                
                    <a href="gallery-masonry-with-filter-3-column.php">
                        <div class="btngalleryarchivedestination rotate"></div>
                    </a>
                    
                    <img alt="" src="img/destinations/photos/img5.jpg">
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivedestination">
                
                    <div class="titlearchivedestination">
                        <h4>OCEANIA<span class="bulletarchivedestination"></span></h4>
                    </div>	
                    
                    <div class="listarchivedestination">
                    
                        <ul>
                            <li><a href="#"><p>Australia</p></a></li>
                            <li><a href="#"><p>New Zealand</p></a></li>
                            <li><a href="#"><p>Cook Islands</p></a></li>
                            <li><a href="#"><p>Fiji Islands</p></a></li>
                            <li><a href="#"><p>Solomon Islands</p></a></li>
                            <li><a href="#"><p>Lorem Ipsum</p></a></li>
                            <li><a href="#"><p>Dolor Sit</p></a></li>
                        </ul>
                    
                    </div>
                    
                    <!--start footer archivedestinations-->
                    <div class="footerarchivedestination">
                        
                        <div class="galleryarchivedestination">
                            <span>
                                <a class="tooltip" title="Gallery" href="gallery-masonry-with-filter-3-column.php"><img alt="" src="img/destinations/icongalleryarchivedestination.png"></a>
                            </span>
                        </div>
                       
                        <div class="promotionarchivedestination">
                            <span>
                                <a class="tooltip" title="Promotion" href="prices.php"><img alt="" src="img/destinations/iconpromotionarchivedestination.png"></a>
                            </span>
                        </div>
        
                    </div>
                    <!--end footer archivedestinations-->
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archive destination-->
        </div>
        
        <div class="grid_4">
        	<!--start archive destination-->
            <div class="archivedestination archivedestination-2 yellow fade-up animate1">
                
                <!--left-->
                <div class="leftarchivedestination">
                
                    <a href="gallery-masonry-with-filter-3-column.php">
                        <div class="btngalleryarchivedestination rotate"></div>
                    </a>
                    
                    <img alt="" src="img/destinations/photos/img6.jpg">
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivedestination">
                
                    <div class="titlearchivedestination">
                        <h4>S. AMERICA<span class="bulletarchivedestination"></span></h4>
                    </div>	
                    
                    <div class="listarchivedestination">
                    
                        <ul>
                            <li><a href="#"><p>Brazil</p></a></li>
                            <li><a href="#"><p>Argentine</p></a></li>
                            <li><a href="#"><p>Peru</p></a></li>
                            <li><a href="#"><p>Mexico</p></a></li>
                            <li><a href="#"><p>Venezuela</p></a></li>
                            <li><a href="#"><p>Lorem Ipsum</p></a></li>
                            <li><a href="#"><p>Dolor Sit</p></a></li>
                        </ul>
                    
                    </div>
                    
                    <!--start footer archivedestinations-->
                    <div class="footerarchivedestination">
                        
                        <div class="galleryarchivedestination">
                            <span>
                                <a class="tooltip" title="Gallery" href="gallery-masonry-with-filter-3-column.php"><img alt="" src="img/destinations/icongalleryarchivedestination.png"></a>
                            </span>
                        </div>
                       
                        <div class="promotionarchivedestination">
                            <span>
                                <a class="tooltip" title="Promotion" href="prices.php"><img alt="" src="img/destinations/iconpromotionarchivedestination.png"></a>
                            </span>
                        </div>
        
                    </div>
                    <!--end footer archivedestinations-->
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archive destination-->
        </div>
        
 
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			
			$('.header-page').parallax("100%", 0.1);

		});
		//end parallax
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		
		
		//start destination height and scroll
		$(document).ready(function() {
		
			//height
			var qntarchivedestination = $('.archivedestination').length;
			setInterval(function(){
				
				i=0;
	
				while ( i < qntarchivedestination ){
				
					//title and img hometours height
					var leftarchivedestinationheight = $(".archivedestination-"+i+" .leftarchivedestination").height();
					var titlearchivedestinationheight = $(".archivedestination-"+i+" .titlearchivedestination").height();
					var footerarchivedestinationheight = $(".archivedestination-"+i+" .footerarchivedestination").height();
			
					$(".archivedestination-"+i+" .listarchivedestination").css({
					  "height": leftarchivedestinationheight - titlearchivedestinationheight - footerarchivedestinationheight
					});
					
					i++;
				
				}
	
			}, 0);
			
			//scroll
			$(".listarchivedestination").niceScroll({
				touchbehavior:true,
				cursorcolor:"#EBEEF2",
				cursoropacitymax:0.9,
				cursorwidth:3,
				autohidemode:true,
				cursorborder:"0px solid #2848BE",
				cursorborderradius:"0px"
				
			});
		});
		//end destination height and scroll
		

		/* ]]> */
	</script>
    
</body>  
</html>