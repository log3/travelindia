<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-gallery">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">GALLERY<br/><span class="header-pagedescription">with 3 columns</span></h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    

		<div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery violet">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img1.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img1.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
        
        <div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery blue">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img2.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img2.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
        
        <div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery green">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img3.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img3.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
        
        <div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery yellow">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img4.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img4.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
        
        <div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery orange">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img5.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img5.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
        
        <div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery green">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img6.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img6.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
        
        <div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery blue">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img7.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img7.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
        
        <div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery green">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img8.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img8.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
        
        <div class="grid_4 fade-up animate1">
        	<!--start image gallery-->
            <div class="singleimagegallery violet">
            
                <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img9.jpg">
                    <div class="btngallerypage rotate"></div>
                </a>
                
                <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img9.jpg">
                
                <div class="titlesingleimagegallery">
                    <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                </div>

            </div>
            <!--end image gallery-->
        </div>
            
        
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/fancybox/jquery.fancybox.js"></script> <!--main fancybox-->
    <script src="js/fancybox/jquery.fancybox-thumbs.js"></script> <!--fancybox thumbs-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//start fancybox
		$(document).ready(function(){
						
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : true,
				arrows    : true,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
			
		});
		//end fancybox
		

		/* ]]> */
	</script>
    
</body>  
</html>