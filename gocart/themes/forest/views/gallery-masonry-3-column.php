<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-gallery">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">GALLERY MASONRY<br/><span class="header-pagedescription">with 3 columns</span></h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    
    
		<!--start blogmasonry-->
		<div class="stylemasonry fade-down animate1">
    

            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery violet">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img1.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img1.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery blue">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img2.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img2-vertical.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery green">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img3.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img3.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery yellow">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img4.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img4.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery orange">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img5.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img5-vertical.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery green">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img6.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img6-vertical.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery blue">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img7.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img7.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery green">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img8.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img8.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_4 singlemasonry">
                <!--start image gallery-->
                <div class="singleimagegallery violet">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img9.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img9.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
    	</div>
        <!--end masonry-->
    
    
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/fancybox/jquery.fancybox.js"></script> <!--main fancybox-->
    <script src="js/fancybox/jquery.fancybox-thumbs.js"></script> <!--fancybox thumbs-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
    <script src="js/isotope/jquery.isotope.min.js"></script> <!--isotope-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//start masonry
		jQuery(document).ready(function() {

			$(function(){
						  
			  //initialize
			  var $container = $('.stylemasonry');
			  

			  $container.isotope({
				itemSelector : '.singlemasonry'
			  });
			  //end initialize
			  
			  
			  //start masonry
			  $container.isotope({
				itemSelector : '.singlemasonry'
			  });
			  // end masonry
			  

			 //relayout 
			setInterval(function(){
				$container.isotope('reLayout');
			}, 0);
			//reLayout
			
			

			});		   

		});
		//start masonry
		
		
		//start fancybox
		$(document).ready(function(){
						
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : true,
				arrows    : true,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
			
		});
		//end fancybox
		

		/* ]]> */
	</script>
    
</body>  
</html>