<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-gallery">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">GALLERY WITH FILTER<br/><span class="header-pagedescription">with 2 columns</span></h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    
    	<!--start options-->
        <div class="grid_12 gridoptions fade-down animate1">
            <div id="options" class="clear">
                <ul id="filters" class="option-set clearfix" data-option-key="filter">
                    <li><a class="selected" href="#filter" data-option-value="*">SHOW ALL</a><span>6</span></li>
                    <li><a href="#filter" data-option-value=".filterone">Filter 1</a></li>
                    <li><a href="#filter" data-option-value=".filtertwo">Filter 2</a></li>
                    <li><a href="#filter" data-option-value=".filterthree">Filter 3</a></li>
                </ul>
            </div>
        </div>
        <!--end options-->
        
        
        <!--start isotope filter-->
        <div class="isotope-filter fade-up animate1">
        
            <div class="grid_6 single-isotope-filter filterone">
                <!--start image gallery-->
                <div class="singleimagegallery violet">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img1.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img1.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_6 single-isotope-filter filterone">
                <!--start image gallery-->
                <div class="singleimagegallery blue">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img2.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img2.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_6 single-isotope-filter filtertwo">
                <!--start image gallery-->
                <div class="singleimagegallery green">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img3.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img3.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_6 single-isotope-filter filtertwo">
                <!--start image gallery-->
                <div class="singleimagegallery yellow">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img4.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img4.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_6 single-isotope-filter filterthree">
                <!--start image gallery-->
                <div class="singleimagegallery orange">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img5.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img5.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>
            
            <div class="grid_6 single-isotope-filter filterthree">
                <!--start image gallery-->
                <div class="singleimagegallery green">
                
                    <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/standard/big/img6.jpg">
                        <div class="btngallerypage rotate"></div>
                    </a>
                    
                    <img alt="" class="imgsingleimagegallery" src="img/gallery/standard/little/img6.jpg">
                    
                    <div class="titlesingleimagegallery">
                        <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                    </div>
    
                </div>
                <!--end image gallery-->
            </div>

        </div>
        <!--end isotope filter-->     
        
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/fancybox/jquery.fancybox.js"></script> <!--main fancybox-->
    <script src="js/fancybox/jquery.fancybox-thumbs.js"></script> <!--fancybox thumbs-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
    <script src="js/isotope/jquery.isotope.min.js"></script> <!--isotope-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//isotope filter
		jQuery(document).ready(function() {
		
			var $container = $('.isotope-filter');

			$container.isotope({
			itemSelector : '.single-isotope-filter'
			});

			//relayout 
			setInterval(function(){
				$container.isotope('reLayout');
			}, 0);
			//reLayout


			var $optionSets = $('#options .option-set'),
			  $optionLinks = $optionSets.find('a');

			$optionLinks.click(function(){
			var $this = $(this);
			// don't proceed if already selected
			if ( $this.hasClass('selected') ) {
			  return false;
			}
			var $optionSet = $this.parents('.option-set');
			$optionSet.find('.selected').removeClass('selected');
			$this.addClass('selected');

			// make option object dynamically, i.e. { filter: '.my-filter-class' }
			var options = {},
				key = $optionSet.attr('data-option-key'),
				value = $this.attr('data-option-value');
			// parse 'false' as false boolean
			value = value === 'false' ? false : value;
			options[ key ] = value;
			if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
			  // changes in layout modes need extra logic
			  changeLayoutMode( $this, options )
			} else {
			  // otherwise, apply new options
			  $container.isotope( options );
			}

			return false;
			});
		
		});
		//end isotope filter
		
		
		//start fancybox
		$(document).ready(function(){
						
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : true,
				arrows    : true,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
			
		});
		//end fancybox
		

		/* ]]> */
	</script>
    
</body>  
</html>