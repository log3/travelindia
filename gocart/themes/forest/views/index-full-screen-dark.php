<?php include "include/header-dark.php"; ?>
<?php include "include/home/slide-full-screen.php"; ?>

	<!--start copyright-->
    <section id="copyright">
        
        <!--start container-->
        <div class="container">
        
            <div class="grid_12">
                <p>© Copyright 2013 by Nicdark - All Rights Reserved</p>   
            </div>
    
        </div>
        <!--end container-->
        
    </section>
    <!--end copyright-->

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script> <!--rev slider-->
    <script src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script> <!--rev slider-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		
		
		//start revolution slider
		var revapis;

		jQuery(document).ready(function() {

			revapis = jQuery('.tp-banner-full-screen').revolution(
			{
				delay:9000,
				startwidth:1170,
				startheight:650,
				hideThumbs:10,
				navigationType:"none",
				fullWidth:"off",
				fullScreen:"on",
				fullScreenOffsetContainer: "#copyright"
			});

		});	//ready
		//end revolution slider

		
		/* ]]> */
	</script>
	
    
</body>  
</html>