<?php include "include/header.php"; ?>
<?php include "include/home/slide-full-width.php"; ?>
<?php include "include/home/services.php"; ?>

<!--start section-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
    
    
		<div class="grid_12"></div><div class="grid_12"></div> <!--40px height-->
		
		
		<div class="grid_12">
			<div class="titlesection blue">
				<h2 class="titlewithborder"><span>OUR AGENCY</span></h2>
			</div>  
		</div>
				
				
		<div class="grid_6 blue fade-left animate1">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl</p><br/>
			
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl</p>
		</div>

		<div class="grid_6 fade-right animate1">
			<div class="progressbar blue fade-right animate1">
				<h4 class="progressbartitle" style="width:100%"><span>HONEYMOON - 100%</span></h4>   
			</div>
			<div class="progressbar green fade-right animate2">
				<h4 class="progressbartitle" style="width:70%"><span>PACKAGE TOURS - 70%</span></h4>   
			</div>  
			<div class="progressbar orange fade-right animate3">
				<h4 class="progressbartitle" style="width:60%"><span>FLY AND DRIVE - 60%</span></h4>   
			</div>  
			<div class="progressbar red fade-right animate4">
				<h4 class="progressbartitle" style="width:90%"><span>TRAVEL RELAX - 90%</span></h4>   
			</div> 
		</div>       
                   
        
    </div>
    <!--end container--> 
    
</section>
<!--end section-->



<div class="divider"><span></span></div>


<!--start homedestinations-->
<section class="sectionparallax header-page-tours">

	<!--start container for arrows-->
    <div class="container arrowscarousel clearfix">
        
        <!--start arrows carousel-->
        <div class="grid_6">
            <div id="showbiz_left_2" class="arrowcarouselprev fade-right"></div>
        </div>
        <div class="grid_6">
            <div id="showbiz_right_2" class="arrowcarouselnext fade-left"></div>
        </div>
        <!--end arrows carousel-->
        
    </div>
    <!--end container for arrows-->
    
    <!--start container-->
    <div class="container clearfix showbiz-container">
    
    
    
    	<div class="showbiz" data-left="#showbiz_left_2" data-right="#showbiz_right_2" data-play="#showbiz_play_2">
        	<div class="overflowholder">
            	<ul>
                
                
                	<li>
                    	<!--start destination 1-->
                        <div class="destinationcarousel single-carousel">
                            
                            <img alt="" class="imgdestination" src="img/destinations/destination1.jpg">
                            
                            <!--start avatarandtitle-->
                            <div class="avatarandtitle">
                                
                                <!--start avatar-->
                                <div class="avatardestination">
                                    <img alt="" src="img/destinations/avatar1.jpg">	
                                </div>
                                <!--end avatar-->
                                
                                <!--title-->
                                <p class="titledestination">
                                    <a href="#">Phuket - Thailandia</a>
                                </p>
                                <!--end title-->
                                
                            </div>
                            <!--end avatarandtitle-->
                          
                            <p class="descriptiondestination">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</p>
                            
                            <div class="infodestination">
                            
                                <div class="viewdestination">
                                    <span>
                                        <img alt="" src="img/destinations/viewicon.png">
                                        234
                                    </span>
                                </div>
                               
                                <div class="likedestination">
                                    <span>
                                        <img alt="" src="img/destinations/likeicon.png">
                                        234
                                    </span>
                                </div>
                                
                                <div class="commentsdestination">
                                    <span>
                                        <img alt="" src="img/destinations/commenticon.png">
                                        234
                                    </span>
                                </div>
                            
                            </div>
                               
                        </div>
                         <!--end destination 1-->
                    </li>
                    
                    
                    <li>
                    	<!--start destination 2-->
                        <div class="destinationcarousel single-carousel">
                            
                            <img alt="" class="imgdestination" src="img/destinations/destination2.jpg">
                            
                            <!--start avatarandtitle-->
                            <div class="avatarandtitle">
                                
                                <!--start avatar-->
                                <div class="avatardestination">
                                    <img alt="" src="img/destinations/avatar2.jpg">	
                                </div>
                                <!--end avatar-->
                                
                                <!--title-->
                                <p class="titledestination">
                                    <a href="#">Venice - Italy</a>
                                </p>
                                <!--end title-->
                                
                            </div>
                            <!--end avatarandtitle-->
                            
                            <p class="descriptiondestination">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</p>
                            
                            <div class="infodestination">
                            
                                <div class="viewdestination">
                                    <span>
                                        <img alt="" src="img/destinations/viewicon.png">
                                        234
                                    </span>
                                </div>
                               
                                <div class="likedestination">
                                    <span>
                                        <img alt="" src="img/destinations/likeicon.png">
                                        234
                                    </span>
                                </div>
                                
                                <div class="commentsdestination">
                                    <span>
                                        <img alt="" src="img/destinations/commenticon.png">
                                        234
                                    </span>
                                </div>
                            
                            </div>
                               
                        </div>
                        <!--end destination 2-->	
                    </li>
                    
                    
                    <li>
                    	<!--start destination 3-->
                        <div class="destinationcarousel single-carousel">
                            
                            <img alt="" class="imgdestination" src="img/destinations/destination3.jpg">
                            
                            <!--start avatarandtitle-->
                            <div class="avatarandtitle">
                                
                                <!--start avatar-->
                                <div class="avatardestination">
                                    <img alt="" src="img/destinations/avatar3.jpg">	
                                </div>
                                <!--end avatar-->
                                
                                <!--title-->
                                <p class="titledestination">
                                    <a href="#">Temple - Thailand</a>
                                </p>
                                <!--end title-->
                                
                            </div>
                            <!--end avatarandtitle-->
                            
                            <p class="descriptiondestination">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</p>
                            
                            <div class="infodestination">
                            
                                <div class="viewdestination">
                                    <span>
                                        <img alt="" src="img/destinations/viewicon.png">
                                        234
                                    </span>
                                </div>
                               
                                <div class="likedestination">
                                    <span>
                                        <img alt="" src="img/destinations/likeicon.png">
                                        234
                                    </span>
                                </div>
                                
                                <div class="commentsdestination">
                                    <span>
                                        <img alt="" src="img/destinations/commenticon.png">
                                        234
                                    </span>
                                </div>
                            
                            </div>
                               
                        </div>
                        <!--end destination 3-->	
                    </li>
                    
                    
                    <li>
                    	<!--start destination 4-->
                        <div class="destinationcarousel single-carousel">
                            
                            <img alt="" class="imgdestination" src="img/destinations/destination4.jpg">
                            
                            <!--start avatarandtitle-->
                            <div class="avatarandtitle">
                                
                                <!--start avatar-->
                                <div class="avatardestination">
                                    <img alt="" src="img/destinations/avatar4.jpg">	
                                </div>
                                <!--end avatar-->
                                
                                <!--title-->
                                <p class="titledestination">
                                    <a href="#">Barcelona - Spain</a>
                                </p>
                                <!--end title-->
                                
                            </div>
                            <!--end avatarandtitle-->
                            
                            <p class="descriptiondestination">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</p>
                            
                            <div class="infodestination">
                            
                                <div class="viewdestination">
                                    <span>
                                        <img alt="" src="img/destinations/viewicon.png">
                                        234
                                    </span>
                                </div>
                               
                                <div class="likedestination">
                                    <span>
                                        <img alt="" src="img/destinations/likeicon.png">
                                        234
                                    </span>
                                </div>
                                
                                <div class="commentsdestination">
                                    <span>
                                        <img alt="" src="img/destinations/commenticon.png">
                                        234
                                    </span>
                                </div>
                            
                            </div>
                               
                        </div>
                        <!--end destination 4-->
                    </li>
                    
                    
                    <li>
                    	<!--start destination 5-->
                        <div class="destinationcarousel single-carousel">
                            
                            <img alt="" class="imgdestination" src="img/destinations/destination1.jpg">
                            
                            <!--start avatarandtitle-->
                            <div class="avatarandtitle">
                                
                                <!--start avatar-->
                                <div class="avatardestination">
                                    <img alt="" src="img/destinations/avatar1.jpg">	
                                </div>
                                <!--end avatar-->
                                
                                <!--title-->
                                <p class="titledestination">
                                    <a href="#">Phuket - Thailandia</a>
                                </p>
                                <!--end title-->
                                
                            </div>
                            <!--end avatarandtitle-->
                            
                            <p class="descriptiondestination">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</p>
                            
                            <div class="infodestination">
                            
                                <div class="viewdestination">
                                    <span>
                                        <img alt="" src="img/destinations/viewicon.png">
                                        234
                                    </span>
                                </div>
                               
                                <div class="likedestination">
                                    <span>
                                        <img alt="" src="img/destinations/likeicon.png">
                                        234
                                    </span>
                                </div>
                                
                                <div class="commentsdestination">
                                    <span>
                                        <img alt="" src="img/destinations/commenticon.png">
                                        234
                                    </span>
                                </div>
                            
                            </div>
                               
                        </div>
                        <!--end destination 5-->
                    </li>
                    
                    <li>
                    	<!--start destination 6-->
                        <div class="destinationcarousel single-carousel">
                            
                            <img alt="" class="imgdestination" src="img/destinations/destination2.jpg">
                            
                            <!--start avatarandtitle-->
                            <div class="avatarandtitle">
                                
                                <!--start avatar-->
                                <div class="avatardestination">
                                    <img alt="" src="img/destinations/avatar2.jpg">	
                                </div>
                                <!--end avatar-->
                                
                                <!--title-->
                                <p class="titledestination">
                                    <a href="#">Venice - Italy</a>
                                </p>
                                <!--end title-->
                                
                            </div>
                            <!--end avatarandtitle-->
                            
                            <p class="descriptiondestination">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</p>
                            
                            <div class="infodestination">
                            
                                <div class="viewdestination">
                                    <span>
                                        <img alt="" src="img/destinations/viewicon.png">
                                        234
                                    </span>
                                </div>
                               
                                <div class="likedestination">
                                    <span>
                                        <img alt="" src="img/destinations/likeicon.png">
                                        234
                                    </span>
                                </div>
                                
                                <div class="commentsdestination">
                                    <span>
                                        <img alt="" src="img/destinations/commenticon.png">
                                        234
                                    </span>
                                </div>
                            
                            </div>
                               
                        </div>
                        <!--end destination 6-->
                    </li>
                
                
                </ul>
          	</div>
   		</div>
    
		
    </div>
    <!--end container--> 
    
</section>
<!--end homedestinations-->




<!--start page-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
    
            
            <div class="grid_12"></div><div class="grid_12"></div> <!--40px height-->
            
            
            <div class="grid_12">
                <div class="titlesection green">
                    <h2 class="titlewithborder"><span>OUR GALLERY</span></h2>
                </div>  
            </div>
            
            
            <!--start options-->
            <div class="grid_12 gridoptions fade-down animate1">
                <div id="options" class="clear">
                    <ul id="filters" class="option-set clearfix" data-option-key="filter">
                        <li><a class="selected" href="#filter" data-option-value="*">SHOW ALL</a><span>12</span></li>
                        <li><a href="#filter" data-option-value=".filterone">Filter 1</a></li>
                        <li><a href="#filter" data-option-value=".filtertwo">Filter 2</a></li>
                        <li><a href="#filter" data-option-value=".filterthree">Filter 3</a></li>
                    </ul>
                </div>
            </div>
            <!--end options-->
            
            
            <!--start isotope filter-->
			<div class="isotope-filter stylemasonry fade-down animate1">
			
                <div class="grid_3 singlemasonry single-isotope-filter filterone">
                    <!--start image gallery-->
                    <div class="singleimagegallery violet">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img1.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img1.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterone">
                    <!--start image gallery-->
                    <div class="singleimagegallery blue">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img2.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img2-vertical.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterone">
                    <!--start image gallery-->
                    <div class="singleimagegallery green">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img3.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img3.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterone">
                    <!--start image gallery-->
                    <div class="singleimagegallery yellow">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img4.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img4.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filtertwo">
                    <!--start image gallery-->
                    <div class="singleimagegallery orange">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img5.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img5-vertical.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filtertwo">
                    <!--start image gallery-->
                    <div class="singleimagegallery green">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img6.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img6-vertical.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filtertwo">
                    <!--start image gallery-->
                    <div class="singleimagegallery blue">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img7.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img7.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filtertwo">
                    <!--start image gallery-->
                    <div class="singleimagegallery green">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img8.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img8.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterthree">
                    <!--start image gallery-->
                    <div class="singleimagegallery violet">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img9.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img9-vertical.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterthree">
                    <!--start image gallery-->
                    <div class="singleimagegallery blue">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img10.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img10.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                
                <div class="grid_3 singlemasonry single-isotope-filter filterthree">
                    <!--start image gallery-->
                    <div class="singleimagegallery green">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img11.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img11.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterthree">
                    <!--start image gallery-->
                    <div class="singleimagegallery yellow">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img12.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img12.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
    		
			</div>
            <!--end isotope filter-->        
        
    </div>
    <!--end container--> 
    
</section>

<div class="divider"><span></span></div>

<section class="sectionparallax header-page-single-project">

	<!--start container-->
    <div class="container clearfix">
        
        
		<div class="grid_12">
			<div class="titlesection white">
				<h2 class="titlewithborder"><span>OUR TEAM</span></h2>
			</div>  
		</div>
		
		
		<div class="grid_3">
        	<!--start member-->
            <div class="member green fade-left animate1">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member1.jpg">
                </div>
                <h4 class="membername">JANE MC DOE</h4>
                <p class="memberposition"><i>Owner</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member blue fade-left animate2">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member2.jpg">
                </div>
                <h4 class="membername">NICK HOPE</h4>
                <p class="memberposition"><i>Cofounder</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member green fade-left animate3">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member3.jpg">
                </div>
                <h4 class="membername">JULIETTE LIGHT</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member blue fade-left animate4">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member4.jpg">
                </div>
                <h4 class="membername">MARK SPITCH</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
	</div>
    <!--end container-->
   
  
            
</section>


<!--start page-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
    
    	
		<div class="grid_12"></div><div class="grid_12"></div> <!--40px height-->
        
        
        <div class="grid_12">
            <div class="titlesection red">
                <h2 class="titlewithborder"><span>OUR BLOG</span></h2>
            </div>  
        </div>

		<!--start blogmasonry-->
		<div class="stylemasonry">

        
            <!--start post masonry-->
            <div class="grid_4 singlemasonry singlepostmasonry red">
                
                <img alt="" class="imgsinglepostmasonry" src="img/gallery/masonry/big/img5.jpg">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_4 singlemasonry singlepostmasonry yellow">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_4 singlemasonry singlepostmasonry green">
                
                <!--soundcloud-->
                <iframe class="soundcloudiframe" height="166" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/110614747"></iframe>
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_4 singlemasonry singlepostmasonry blue">
                
                <img alt="" class="imgsinglepostmasonry" src="img/gallery/masonry/big/img10.jpg">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_4 singlemasonry singlepostmasonry violet">
                
                <img alt="" class="imgsinglepostmasonry" src="img/gallery/masonry/big/img9.jpg">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            <!--start post masonry-->
            <div class="grid_4 singlemasonry singlepostmasonry orange">
                
                <!--vimeo-->
                <iframe class="vimeoiframe" src="//player.vimeo.com/video/79623167?title=0&amp;byline=0&amp;portrait=0&amp;color=ff9933" width="500" height="281"></iframe>
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            
            <!--start post masonry-->
            <div class="grid_4 singlemasonry singlepostmasonry red">
                
                <div class="titledaysinglepostmasonry">
                    
                    <p class="titlesinglepostmasonry">LOREM IPSUM</p>
                
                    <div class="daysinglepostmasonry">
                        <p>15</p>
                        <span>MAY</span>
                    </div>
                
                </div> 
                
                <p class="descriptionsinglepostmasonry">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus at cursus et, gravida quis nisl.</p>   
                   
                <a class="areadmoresinglepostmasonry" href="#"><p class="readmoresinglepostmasonry">READ MORE</p></a>
            
            </div>
            <!--end post masonry-->
            
            
        </div>
        <!--end blogmasonry-->
        

        <!--start archive pagination-->
        <div class="contentarchivepagination paginationmasonry">
            <div class="archivepagination">
                <a title="Previous Posts" class="btn prevbtn tooltip" href="#"><img alt="" src="img/blog/prevpagination.png"></a>
                <a title="Next Posts" class="btn nextbtn tooltip" href="blog-masonry-with-3-column-2.php"><img alt="" src="img/blog/nextpagination.png"></a>
            </div>
        </div>
        <!--end archive pagination-->
        
        
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<section class="sectionparallax header-page-prices">

	<!--start container-->
    <div class="container clearfix">
    

        <div class="grid_12">
			<div class="titlesection white">
				<h2 class="titlewithborder"><span>OUR PRICES</span></h2>
			</div>  
		</div>
		
		
		<div class="grid_3">
        	<div class="price blue fade-left animate1">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 30 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price violet fade-left animate2">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 50 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price green fade-left animate3">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 70 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price blue fade-left animate4">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 40 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        

    </div>
    <!--end container--> 
    
</section>



<!--start homeclients-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
            
        
		<div class="grid_12"></div><div class="grid_12"></div> <!--40px height-->
        
        
        <div class="grid_12">
            <div class="titlesection orange">
                <h2 class="titlewithborder"><span>OUR CLIENTS</span></h2> 
            </div>  
        </div>
       
        <div class="grid_3">
        	<div class="imgclient fade-left animate1">
                <img alt="" src="img/clients/client1.png">    
            </div>
        </div>
        
       	<div class="grid_3">
            <div class="imgclient fade-left animate2">
                <img alt="" src="img/clients/client2.png">     
            </div>
        </div>
        
        <div class="grid_3">
            <div class="imgclient fade-left animate3">
                <img alt="" src="img/clients/client3.png">    
            </div>
        </div>
        
        <div class="grid_3">
            <div class="imgclient fade-left animate4">
                <img alt="" src="img/clients/client4.png">        
            </div>
        </div>
    
    </div>
    <!--end container--> 
    
</section>
<!--end homeclients-->


<div class="divider"><span></span></div>


<!--start contact-->
<div class="contactmap">
	
    <!--start info contact-->
    <div class="infocontact infocontactlight blue">
    
    	<div class="contentinfocontact">
        	<p class="titleinfocontact">LOVE TRAVEL</p>
            <ul>
            	<li><p><img alt="" src="img/contact/iconinfocontactaddress.png">102 Madison Ave - New York ( NY )</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactphone.png">Telephone: (0039) 34916656398</p></li>
                <li><p><img alt="" src="img/contact/iconinfocontactmail.png">Mail: yourname@yourdomain.xx</p></li>
            </ul>
        </div>
        
        <div class="triangleinfocontact">
        	<span></span>
        </div>
        
    	<!--start marker-->
        <div class="markercontactmap">
            <div class="circlemarker"><div class="innercirclemarker"></div></div>
            <div class="trianglemarker"></div>
        </div>
        <!--end marker-->
    
    </div>
    <!--end info contact-->
    
	<!--google maps-->
    <div id="map-canvas" class="map-canvas-dark"></div>
    <!--google maps-->
    	
</div>
<!--end contactmap-->
<!--start copyright-->
    <section class="copyright-for-404" id="copyright">
        
        <!--start container-->
        <div class="container">
        
            <div class="grid_12">
                <p>© Copyright 2013 by Nicdark - All Rights Reserved</p>   
            </div>
    
        </div>
        <!--end container-->
        
        <div class="backtotop">
        	<a href="#startpage"><img alt="" src="img/footer/arrowbacktotop.png" /></a>
        </div> 
        
    </section>
    <!--end copyright-->
    
    
    <!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script> <!-- Google Map API -->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script> <!--rev slider-->
    <script src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script> <!--rev slider-->
    <script type="text/javascript" src="showbizpro/js/jquery.themepunch.plugins.min.js"></script> <!--showbiz-->						
	<script type="text/javascript" src="showbizpro/js/jquery.themepunch.showbizpro.min.js"></script> <!--showbiz-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.easy-pie-chart.js"></script> <!--Chart-->
    <script src="js/fancybox/jquery.fancybox.js"></script> <!--main fancybox-->
    <script src="js/fancybox/jquery.fancybox-thumbs.js"></script> <!--fancybox thumbs-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
    <script src="js/isotope/jquery.isotope.min.js"></script> <!--isotope-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		
		
		//start carousel
		jQuery(document).ready(function() {

			jQuery('.showbiz-container').showbizpro({
				dragAndScroll:"on",
				visibleElementsArray:[4,3,2,1]
			});
		   
		});
		//end carousel
		
		
		//start revolution slider
		var revapi;

		jQuery(document).ready(function() {

			   revapi = jQuery('.tp-banner-full-width').revolution(
				{
					delay:9000,
					startwidth:1170,
					startheight:650,
					hideThumbs:10,
					navigationType:"none",
					fullWidth:"on",
					forceFullWidth:"on"
				});

		});	//ready
		//end revolution slider
		
		
		//start chart
		$(document).ready(function(){
						
			$('.percentagehome').easyPieChart({
				size: 140,
				rotate: 0,
				lineWidth: 10,
				animate: 1000,
				barColor: '#55738F',
				trackColor: 'transparent',
				scaleColor: false,
				lineCap: 'butt',
			});

		});
		//end chart
		
		
		//start parallax
		jQuery(document).ready(function() {
			
			$('.sectionparallax').parallax("100%", 0.04);

		});
		//end parallax
		
		
		//isotope filter
		jQuery(document).ready(function() {
		
			var $container = $('.isotope-filter');

			$container.isotope({
			itemSelector : '.single-isotope-filter'
			});

			//relayout 
			setInterval(function(){
				$container.isotope('reLayout');
			}, 0);
			//reLayout


			var $optionSets = $('#options .option-set'),
			  $optionLinks = $optionSets.find('a');

			$optionLinks.click(function(){
			var $this = $(this);
			// don't proceed if already selected
			if ( $this.hasClass('selected') ) {
			  return false;
			}
			var $optionSet = $this.parents('.option-set');
			$optionSet.find('.selected').removeClass('selected');
			$this.addClass('selected');

			// make option object dynamically, i.e. { filter: '.my-filter-class' }
			var options = {},
				key = $optionSet.attr('data-option-key'),
				value = $this.attr('data-option-value');
			// parse 'false' as false boolean
			value = value === 'false' ? false : value;
			options[ key ] = value;
			if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
			  // changes in layout modes need extra logic
			  changeLayoutMode( $this, options )
			} else {
			  // otherwise, apply new options
			  $container.isotope( options );
			}

			return false;
			});
		
		});
		//end isotope filter
		
		
		//start masonry
		jQuery(document).ready(function() {

			$(function(){
						  
			  //initialize
			  var $container = $('.stylemasonry');
			  

			  $container.isotope({
				itemSelector : '.singlemasonry'
			  });
			  //end initialize
			  
			  
			  //start masonry
			  $container.isotope({
				itemSelector : '.singlemasonry'
			  });
			  // end masonry
			  
			  //relayout 
			setInterval(function(){
				$container.isotope('reLayout');
			}, 0);
			//reLayout
			
			});		   

		});
		//start masonry
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		
		
		//start fancybox
		$(document).ready(function(){
						
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : true,
				arrows    : true,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
			
		});
		//end fancybox
		
		
		//start map
		$(document).ready(function(){
		
			var map;
			var brooklyn = new google.maps.LatLng(40.759277, -73.977064);
			
			var MY_MAPTYPE_ID = 'custom_style';
			
			function initialize() {
				
				
			  var mapcanvasdark = $('.map-canvas-dark').length;
			
			  
			  if (mapcanvasdark==1){
				  
				var featureOpts = [
			 
				  {
					"stylers": [
					  { "saturation": -100 }
					]
				  },{
				  }
			  ];	
				  
			  }else{
				  
				var featureOpts = [];	  
				  
			  }
			  
			  var mapOptions = {
				zoom: 14,
				draggable: false,
				center: brooklyn,
				disableDefaultUI: true,
				scrollwheel: false,
				mapTypeControlOptions: {
				  mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
				},
				mapTypeId: MY_MAPTYPE_ID
			  };
			
			  map = new google.maps.Map(document.getElementById('map-canvas'),
				  mapOptions);
			
			  var styledMapOptions = {
				name: 'Custom Style'
			  };
			
			  var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
			
			  map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
			}
			
			google.maps.event.addDomListener(window, 'load', initialize);
		
		});
		//end map
		
		
		/* ]]> */
	</script>
    
</body>  
</html>