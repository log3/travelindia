<?php include "include/header-dark.php"; ?>
<?php include "include/home/slide-full-screen.php"; ?>
<?php include "include/home/services.php"; ?>


<!--start section-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
    
    
		<div class="dividerheight40"></div>
		<div class="dividerheight10"></div>
		
		
		<div class="grid_12">
			<div class="titlesection blue">
				<h2 class="titlewithborder"><span>OUR AGENCY</span></h2>
			</div>  
		</div>
				
				
		<div class="grid_6 blue fade-left animate1">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl</p><br/>
			
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl</p>
		</div>

		<div class="grid_6 fade-right animate1">
			<div class="progressbar blue fade-right animate1">
				<h4 class="progressbartitle" style="width:100%"><span>HONEYMOON - 100%</span></h4>   
			</div>
			<div class="progressbar green fade-right animate2">
				<h4 class="progressbartitle" style="width:70%"><span>PACKAGE TOURS - 70%</span></h4>   
			</div>  
			<div class="progressbar orange fade-right animate3">
				<h4 class="progressbartitle" style="width:60%"><span>FLY AND DRIVE - 60%</span></h4>   
			</div>  
			<div class="progressbar red fade-right animate4">
				<h4 class="progressbartitle" style="width:90%"><span>TRAVEL RELAX - 90%</span></h4>   
			</div> 
		</div>       
                   
        
    </div>
    <!--end container--> 
    
</section>
<!--end section-->



<div class="divider"><span></span></div>


<!--start page-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
            
			
            <div class="grid_12">
                <div class="titlesection green">
                    <h2 class="titlewithborder"><span>OUR GALLERY</span></h2>
                </div>  
            </div>
            
            
            <!--start options-->
            <div class="grid_12 gridoptions fade-down animate1">
                <div id="options" class="clear">
                    <ul id="filters" class="option-set clearfix" data-option-key="filter">
                        <li><a class="selected" href="#filter" data-option-value="*">SHOW ALL</a><span>12</span></li>
                        <li><a href="#filter" data-option-value=".filterone">Filter 1</a></li>
                        <li><a href="#filter" data-option-value=".filtertwo">Filter 2</a></li>
                        <li><a href="#filter" data-option-value=".filterthree">Filter 3</a></li>
                    </ul>
                </div>
            </div>
            <!--end options-->
            
            
            <!--start isotope filter-->
			<div class="isotope-filter stylemasonry fade-down animate1">
			
                <div class="grid_3 singlemasonry single-isotope-filter filterone">
                    <!--start image gallery-->
                    <div class="singleimagegallery violet">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img1.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img1.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterone">
                    <!--start image gallery-->
                    <div class="singleimagegallery blue">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img2.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img2-vertical.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterone">
                    <!--start image gallery-->
                    <div class="singleimagegallery green">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img3.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img3.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterone">
                    <!--start image gallery-->
                    <div class="singleimagegallery yellow">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img4.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img4.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filtertwo">
                    <!--start image gallery-->
                    <div class="singleimagegallery orange">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img5.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img5-vertical.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filtertwo">
                    <!--start image gallery-->
                    <div class="singleimagegallery green">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img6.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img6-vertical.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filtertwo">
                    <!--start image gallery-->
                    <div class="singleimagegallery blue">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img7.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img7.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filtertwo">
                    <!--start image gallery-->
                    <div class="singleimagegallery green">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img8.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img8.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterthree">
                    <!--start image gallery-->
                    <div class="singleimagegallery violet">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img9.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img9-vertical.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterthree">
                    <!--start image gallery-->
                    <div class="singleimagegallery blue">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img10.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img10.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                
                <div class="grid_3 singlemasonry single-isotope-filter filterthree">
                    <!--start image gallery-->
                    <div class="singleimagegallery green">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img11.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img11.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
                
                <div class="grid_3 singlemasonry single-isotope-filter filterthree">
                    <!--start image gallery-->
                    <div class="singleimagegallery yellow">
                    
                        <a class="fancybox-thumbs" data-fancybox-group="gallery-1" href="img/gallery/masonry/big/img12.jpg">
                            <div class="btngallerypage rotate"></div>
                        </a>
                        
                        <img alt="" class="imgsingleimagegallery" src="img/gallery/masonry/little/img12.jpg">
                        
                        <div class="titlesingleimagegallery">
                            <p>Lorem Ipsum<span class="bulletgallery"></span></p>
                        </div>
        
                    </div>
                    <!--end image gallery-->
                </div>
    		
			</div>
            <!--end isotope filter-->        
        
    </div>
    <!--end container--> 
    
</section>


<div class="divider"><span></span></div>


<section class="sectionparallax header-page-single-project">

	<!--start container-->
    <div class="container clearfix">
        
        
		<div class="grid_12">
			<div class="titlesection white">
				<h2 class="titlewithborder"><span>OUR TEAM</span></h2>
			</div>  
		</div>
		
		
		<div class="grid_3">
        	<!--start member-->
            <div class="member green fade-left animate1 opacitybg">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member1.jpg">
                </div>
                <h4 class="membername">JANE MC DOE</h4>
                <p class="memberposition"><i>Owner</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook-white.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter-white.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram-white.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble-white.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member blue fade-left animate2 opacitybg">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member2.jpg">
                </div>
                <h4 class="membername">NICK HOPE</h4>
                <p class="memberposition"><i>Cofounder</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook-white.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter-white.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram-white.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble-white.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member green fade-left animate3 opacitybg">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member3.jpg">
                </div>
                <h4 class="membername">JULIETTE LIGHT</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook-white.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter-white.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram-white.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble-white.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member blue fade-left animate4 opacitybg">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member4.jpg">
                </div>
                <h4 class="membername">MARK SPITCH</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook-white.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter-white.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram-white.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble-white.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
	</div>
    <!--end container-->
        
</section>


<!--start homeclients-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
            
        
		<div class="grid_12"></div><div class="grid_12"></div> <!--40px height-->
        
        
        <div class="grid_12">
            <div class="titlesection orange">
                <h2 class="titlewithborder"><span>OUR CLIENTS</span></h2> 
            </div>  
        </div>
       
        <div class="grid_3">
        	<div class="imgclient fade-left animate1">
                <img alt="" src="img/clients/client1.png">    
            </div>
        </div>
        
       	<div class="grid_3">
            <div class="imgclient fade-left animate2">
                <img alt="" src="img/clients/client2.png">     
            </div>
        </div>
        
        <div class="grid_3">
            <div class="imgclient fade-left animate3">
                <img alt="" src="img/clients/client3.png">    
            </div>
        </div>
        
        <div class="grid_3">
            <div class="imgclient fade-left animate4">
                <img alt="" src="img/clients/client4.png">        
            </div>
        </div>
    
    </div>
    <!--end container--> 
    
</section>
<!--end homeclients-->

<div class="divider"><span></span></div>
<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script> <!--rev slider-->
    <script src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script> <!--rev slider-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.easy-pie-chart.js"></script> <!--Chart-->
    <script src="js/fancybox/jquery.fancybox.js"></script> <!--main fancybox-->
    <script src="js/fancybox/jquery.fancybox-thumbs.js"></script> <!--fancybox thumbs-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
	 <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/isotope/jquery.isotope.min.js"></script> <!--isotope-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		
		
		//start revolution slider
		var revapis;

		jQuery(document).ready(function() {

			revapis = jQuery('.tp-banner-full-screen').revolution(
			{
				delay:9000,
				startwidth:1170,
				startheight:650,
				hideThumbs:10,
				navigationType:"none",
				fullWidth:"off",
				fullScreen:"on",
				fullScreenOffsetContainer: "#homeservices"
			});

		});	//ready
		//end revolution slider
		
		
		//start chart
		$(document).ready(function(){
						
			$('.percentagehome').easyPieChart({
				size: 140,
				rotate: 0,
				lineWidth: 10,
				animate: 1000,
				barColor: '#55738F',
				trackColor: 'transparent',
				scaleColor: false,
				lineCap: 'butt',
			});

		});
		//end chart
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		
		
		//isotope filter
		jQuery(document).ready(function() {
		
			var $container = $('.isotope-filter');

			$container.isotope({
			itemSelector : '.single-isotope-filter'
			});

			//relayout 
			setInterval(function(){
				$container.isotope('reLayout');
			}, 0);
			//reLayout


			var $optionSets = $('#options .option-set'),
			  $optionLinks = $optionSets.find('a');

			$optionLinks.click(function(){
			var $this = $(this);
			// don't proceed if already selected
			if ( $this.hasClass('selected') ) {
			  return false;
			}
			var $optionSet = $this.parents('.option-set');
			$optionSet.find('.selected').removeClass('selected');
			$this.addClass('selected');

			// make option object dynamically, i.e. { filter: '.my-filter-class' }
			var options = {},
				key = $optionSet.attr('data-option-key'),
				value = $this.attr('data-option-value');
			// parse 'false' as false boolean
			value = value === 'false' ? false : value;
			options[ key ] = value;
			if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
			  // changes in layout modes need extra logic
			  changeLayoutMode( $this, options )
			} else {
			  // otherwise, apply new options
			  $container.isotope( options );
			}

			return false;
			});
		
		});
		//end isotope filter
		
		
		//start masonry
		jQuery(document).ready(function() {

			$(function(){
						  
			  //initialize
			  var $container = $('.stylemasonry');
			  
			  $container.isotope({
				itemSelector : '.singlemasonry'
			  });
			  //end initialize
			  
			  //start masonry
			  $container.isotope({
				itemSelector : '.singlemasonry'
			  });
			  // end masonry

			});		   

		});
		//start masonry
		
		
		//start fancybox
		$(document).ready(function(){
						
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : true,
				arrows    : true,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
			
		});
		//end fancybox
		
		
		//start parallax
		jQuery(document).ready(function() {
			
			$('.sectionparallax').parallax("100%", 0.04);

		});
		//end parallax
		
		
		/* ]]> */
	</script>
	
    
</body>  
</html>