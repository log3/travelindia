<?php include "include/header.php"; ?>

<section class="header-page fade-up">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">PAGE FULL WIDTH</h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    
    
        <!--start content-->
        <div class="grid_12">
        
            <!--start post-->
            <div class="singlepost red">
                
                <img alt="" class="imgsinglepost" src="img/blog/imgtest1.jpg">
                
               	<!--infosinglepost-->
                <div class="infosinglepost">
                
                	<div class="datesinglepost">
                    	<p>21</p>
                        <span>JAN</span>
                    </div>
                   
                    <div class="iconsinglepost">
                        <p><img alt="" src="img/blog/author.png"><a href="">JOHN DOE</a></p>
                        <p><img alt="" src="img/blog/category.png"><a href="">ADVENTURE</a></p>
                	</div>
                    
                </div>
                <!--end infosinglepost-->
                
                
                <h1 class="titlesinglepost">LOREM IPSUM DOLOR SIT AMET</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl. Vestibulum rhoncus libero quis hendrerit euismod. Nulla hendrerit justo nec sem rhoncus sodales. Nam auctor faucibus erat. Phasellus consectetur, ligula a suscipit aliquet, nisl mi venenatis felis, vitae semper risus neque non massa. Curabitur at mi nec ipsum posuere vulputate et a velit. Vestibulum eleifend lacus vel lacinia sollicitudin. Mauris ultricies mauris sit amet ornare auctor. Vestibulum aliquam mi in nulla egestas ornare. Etiam eu porttitor neque. Proin mattis ipsum sed euismod tempor. Sed pretium turpis ac malesuada suscipit. Sed mollis facilisis tortor, vel mattis elit blandit ac. </p>
                
                <blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc.</blockquote>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat. Quisque orci lectus, sodales at cursus et, gravida quis nisl. Vestibulum rhoncus libero quis hendrerit euismod. Nulla hendrerit justo nec sem rhoncus sodales. Nam auctor faucibus erat. Phasellus consectetur, ligula a suscipit aliquet, nisl mi venenatis felis, vitae semper risus neque non massa. Curabitur at mi nec ipsum posuere vulputate et a velit. Vestibulum eleifend lacus vel lacinia sollicitudin. Mauris ultricies mauris sit amet ornare auctor. Vestibulum aliquam mi in nulla egestas ornare. Etiam eu porttitor neque. Proin mattis ipsum sed euismod tempor. Sed pretium turpis ac malesuada suscipit. Sed mollis facilisis tortor, vel mattis elit blandit ac. </p>
                
                <div class="divider"><span></span></div>
                
                <!--start author-->
               	<div class="authorsinglepost">
                
                	<div class="imgsocialauthorsinglepost">
                    	<img alt="" class="imgauthorsinglepost" src="img/blog/imgauthorsinglepost.jpg">
                        
                        <div class="socialauthorsinglepost">
                            <a href="#"><img alt="" src="img/blog/socialauthorsinglepost-facebook.jpg"></a>
                            <a href="#"><img alt="" src="img/blog/socialauthorsinglepost-twitter.jpg"></a>
                            <a href="#"><img alt="" src="img/blog/socialauthorsinglepost-googleplus.jpg"></a>
                        </div>
                    </div>
                
                	<p class="descriptionauthorsinglepost">
                    	<span class="nameauthorsinglepost">JOHN DOE</span><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh	
                    </p>

                </div>
                <!--end author-->
                
                
            </div>
            <!--end post-->
            
        
        </div>
        <!--end content-->
        
            
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		

		/* ]]> */
	</script>
    
</body>  
</html>