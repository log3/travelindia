<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-prices">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">TABLE PRICES</h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    

        <div class="grid_3">
        	<div class="price yellow fade-left animate1">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 30 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price orange fade-left animate2">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 50 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price red fade-left animate3">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 70 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price violet fade-left animate4">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 40 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        

    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<!--start section-->
<section class="sectionparallax header-page-prices">

	<!--start container-->
    <div class="container clearfix">
    

        <div class="grid_12">
			<div class="titlesection white">
				<h2 class="titlewithborder"><span>TRANSPARENT STYLE</span></h2>
			</div>  
		</div>
		
		
		<div class="grid_3">
        	<div class="price green fade-left animate1 opacitybg">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 30 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon-white.png">
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon-white-2.png">
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price blue fade-left animate2 opacitybg">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 50 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon-white.png">
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon-white-2.png">
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price violet fade-left animate3 opacitybg">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 70 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon-white.png">
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon-white-2.png">
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_3">
        	<div class="price red fade-left animate4 opacitybg">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 40 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon-white.png">
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon-white-2.png">
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        

    </div>
    <!--end container--> 
    
</section>
<!--end section-->

<!--start page-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
    
		<div class="grid_12">
			<div class="dividerheight40"></div>
			<div class="titlesection orange">
				<h2 class="titlewithborder"><span>3 COLUMNS</span></h2>
			</div>  
		</div>
	
        <div class="grid_4">
        	<div class="price red fade-left animate1">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 30 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_4">
        	<div class="price orange fade-left animate2">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 50 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
        
        <div class="grid_4">
        	<div class="price yellow fade-left animate3">
                <h4 class="titleprice">LOREM IPSUM</h4>
                
                <div class="valueprice">
                    <p><span class="currency">$</span> 70 <span class="littledescription">/ por month</span></p>
                    <p class="descriptionprice">- <i>This is a description</i> -</p>	
                </div>
                
                <div class="triangle"></div>
                
                <ul>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li><p>Lorem Ipsum Dolor Sit Amet</p></li>
                    <li class="lastlistitem"><p>Lorem Ipsum Dolor Sit Amet</p></li>
                </ul>
                
                
                <!--start footer homeprice-->
                <div class="footerhomeprices">
                    
                    <div class="contacthomeprice">
                        <a class="tooltip" title="Contact" href="#">
							<span>
								<img alt="" src="img/prices/contacticon.png">
								CONTACT
							</span>
						</a>
                    </div>
                   
                    <div class="morehomeprice">
                        <a class="tooltip" title="More Info" href="#">
							<span>
								<img alt="" src="img/prices/moreicon.png">
								MORE
							</span>
						</a>
                    </div>
    
                </div>
                <!--end footer homeprice-->
                   
            </div>
        </div>
      

    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			
			$('.header-page').parallax("100%", 0.1);
			$('.sectionparallax').parallax("100%", 0.04);

		});
		//end parallax
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		

		/* ]]> */
	</script>
    
</body>  
</html>