<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-single-project">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">SINGLE PROJECT</h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    
    
        <!--start flexslider gallery-->
        <div class="grid_8 green fade-left animate1">
        
            <!--start title and price-->
            <div class="titlesingleproject">
            	<div class="pricesingleproject">
                    <p>2000 $</p>
                </div>
            	<h4>TOUR YUCATAN - MEXICO</h4>
            </div>
            <!--end title and price-->
            
            <!--start content flexslider-->
            <div class="contentflexslider">
            
            	<!--start flexslider-->
                <div class="flexslider flex-slider">
                    <ul class="slides">
                        <li><img alt="" src="img/single-project/slider/big/img1.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/big/img2.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/big/img3.jpg" /></li>
						<li><img alt="" src="img/single-project/slider/big/img1.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/big/img2.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/big/img3.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/big/img1.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/big/img2.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/big/img3.jpg" /></li>
                    </ul>
                </div>
                <!--end flexslider-->
                
                
                <!--start flexslider carousel-->
                <div class="flexslider flex-carousel">
                    <ul class="slides">
                        <li><img alt="" src="img/single-project/slider/thumb/img1.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/thumb/img2.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/thumb/img3.jpg" /></li>
						<li><img alt="" src="img/single-project/slider/thumb/img1.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/thumb/img2.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/thumb/img3.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/thumb/img1.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/thumb/img2.jpg" /></li>
                        <li><img alt="" src="img/single-project/slider/thumb/img3.jpg" /></li>
                    </ul>
                </div>
                <!--end flexslider carousel-->
            
            </div>
            <!--end content flexslider-->
            
        
        </div>
        <!--end flexslider gallery-->
        
        
        <!--start info project-->
        <div class="grid_4 green fade-right animate1">
        
        	<!--title date and day-->
            <div class="datedaysingleproject">
                <p class="datesingleproject">03 TO 17 AUGUST</p>
                <div class="daysingleproject">
                    <p>15</p>
                    <span>DAYS</span>
                </div>
            </div>
            <!--title date and day-->
            
            <!--start content info-->
            <div class="contentinfo">
            	<p><strong>FANTASTIC TOUR</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat.</p>
                <blockquote>Destination: Yucatan (Mexico)<br/>Duration: 15 days, flight included<br/>Price: 2.000 $ per person</blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh, vel sodales sem diam nec nunc. Donec mattis blandit metus ut volutpat.</p><br/>
                <a class="btn" href="#"><p>CONTACT US</p></a>
            </div>
            <!--end content info-->
        
        </div>
        <!--end info project-->
        
        <div class="divider"><span></span></div>
        
	</div>
    <!--end container-->
   
    <!--start container for arrows-->
    <div class="container arrowscarousel green clearfix">
        
        <!--start arrows carousel-->
        <div class="grid_6">
            <div id="showbiz_left_2" class="arrowcarouselprev fade-right"></div>
        </div>
        <div class="grid_6">
            <div id="showbiz_right_2" class="arrowcarouselnext fade-left"></div>
        </div>
        <!--end arrows carousel-->
        
    </div>
    <!--end container for arrows--> 
       
    <!--start carousel-->
    <div class="container clearfix showbiz-container">
    
    
        <div class="showbiz" data-left="#showbiz_left_2" data-right="#showbiz_right_2" data-play="#showbiz_play_2">
            <div class="overflowholder">
                <ul>
                
                
                    <li>
        
                        <!--start first destination-->
                        <div class="destinationsingleproject single-carousel blue">
                            
                            <img alt="" class="imgdestinationsingleproject" src="img/single-project/slider/big/img1.jpg">
                            
                            <div class="titledaydestinationsingleproject">
                                <p class="titledestinationsingleproject">CANCUN</p>
                                <div class="daydestinationsingleproject">
                                    <p>1</p>
                                    <span>DAY</span>
                                </div>
                            </div> 
                            
                            <p class="descriptiondestinationsingleproject">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            
                            <a class="readmoredestinationsingleproject rotate" href="#"></a>  
                               
                        </div>
                        <!--end first destination-->
                        
                    </li>
                    
                    <li>
        
                        <!--start first destination-->
                        <div class="destinationsingleproject single-carousel green">
                            
                            <img alt="" class="imgdestinationsingleproject" src="img/single-project/slider/big/img2.jpg">
                            
                            <div class="titledaydestinationsingleproject">
                                <p class="titledestinationsingleproject">CHICHEN-ITZA</p>
                                <div class="daydestinationsingleproject">
                                    <p>2</p>
                                    <span>DAY</span>
                                </div>
                            </div> 
                            
                            <p class="descriptiondestinationsingleproject">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            
                            <a class="readmoredestinationsingleproject rotate" href="#"></a>   
                               
                        </div>
                        <!--end first destination-->
                        
                    </li>
                    
                    
                    <li>
        
                        <!--start first destination-->
                        <div class="destinationsingleproject single-carousel blue">
                            
                            <img alt="" class="imgdestinationsingleproject" src="img/single-project/slider/big/img3.jpg">
                            
                            <div class="titledaydestinationsingleproject">
                                <p class="titledestinationsingleproject">PLAYA DEL CARMEN</p>
                                <div class="daydestinationsingleproject">
                                    <p>3</p>
                                    <span>DAY</span>
                                </div>
                            </div> 
                            
                            <p class="descriptiondestinationsingleproject">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            
                            <a class="readmoredestinationsingleproject rotate" href="#"></a>   
                               
                        </div>
                        <!--end first destination-->
                        
                    </li>
                    
                    <li>
        
                        <!--start first destination-->
                        <div class="destinationsingleproject single-carousel green">
                            
                            <img alt="" class="imgdestinationsingleproject" src="img/single-project/slider/big/img1.jpg">
                            
                            <div class="titledaydestinationsingleproject">
                                <p class="titledestinationsingleproject">MERIDA</p>
                                <div class="daydestinationsingleproject">
                                    <p>4</p>
                                    <span>DAY</span>
                                </div>
                            </div> 
                            
                            <p class="descriptiondestinationsingleproject">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            
                            <a class="readmoredestinationsingleproject rotate" href="#"></a>   
                               
                        </div>
                        <!--end first destination-->
                        
                    </li>
                    
                    <li>
        
                        <!--start first destination-->
                        <div class="destinationsingleproject single-carousel blue">
                            
                            <img alt="" class="imgdestinationsingleproject" src="img/single-project/slider/big/img2.jpg">
                            
                            <div class="titledaydestinationsingleproject">
                                <p class="titledestinationsingleproject">LOREM IPSUM</p>
                                <div class="daydestinationsingleproject">
                                    <p>5</p>
                                    <span>DAY</span>
                                </div>
                            </div> 
                            
                            <p class="descriptiondestinationsingleproject">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            
                            <a class="readmoredestinationsingleproject rotate" href="#"></a>   
                               
                        </div>
                        <!--end first destination-->
                        
                    </li>
                    
                    
                </ul>
            </div>
        </div>
        

    </div>
    <!--end carousel-->
            
            
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script type="text/javascript" src="showbizpro/js/jquery.themepunch.plugins.min.js"></script> <!--showbiz-->						
	<script type="text/javascript" src="showbizpro/js/jquery.themepunch.showbizpro.min.js"></script> <!--showbiz-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
    <script src="js/jquery.flexslider-min.js"></script> <!--flexslider-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//start flexslider
		jQuery(document).ready(function() {
		  
		  $('.flex-carousel').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 100,
			itemMargin: 5,
			asNavFor: '.flex-slider'
		  });

		  $('.flex-slider').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			sync: ".flex-carousel",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		  
		});
		//end flexslider
		
		
		//start carousel
		jQuery(document).ready(function() {

			jQuery('.showbiz-container').showbizpro({
				dragAndScroll:"on",
				visibleElementsArray:[4,3,2,1]
			});
		   
		});
		//end carousel
		

		/* ]]> */
	</script>
    
</body>  
</html>