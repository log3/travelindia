<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-team">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">MEET OUR TEAM</h2></div>
</section>

<div class="divider"><span></span></div>


<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
        
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member yellow fade-left animate1">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member1.jpg">
                </div>
                <h4 class="membername">JANE MC DOE</h4>
                <p class="memberposition"><i>Owner</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member orange fade-left animate2">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member2.jpg">
                </div>
                <h4 class="membername">NICK HOPE</h4>
                <p class="memberposition"><i>Cofounder</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member red fade-left animate3">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member3.jpg">
                </div>
                <h4 class="membername">JULIETTE LIGHT</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member violet fade-left animate4">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member4.jpg">
                </div>
                <h4 class="membername">MARK SPITCH</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
	</div>
    <!--end container-->
            
</section>
<!--end internal page-->


<div class="divider"><span></span></div>

<!--start section-->
<section class="sectionparallax header-page-prices">

	<!--start container-->
    <div class="container clearfix">
    

        <div class="grid_12">
			<div class="titlesection white">
				<h2 class="titlewithborder"><span>TRANSPARENT STYLE</span></h2>
			</div>  
		</div>
		
		
		<div class="grid_3">
        	<!--start member-->
            <div class="member green fade-left animate1 opacitybg">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member1.jpg">
                </div>
                <h4 class="membername">JANE MC DOE</h4>
                <p class="memberposition"><i>Owner</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook-white.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter-white.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram-white.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble-white.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member blue fade-left animate2 opacitybg">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member2.jpg">
                </div>
                <h4 class="membername">NICK HOPE</h4>
                <p class="memberposition"><i>Cofounder</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook-white.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter-white.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram-white.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble-white.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member violet fade-left animate3 opacitybg">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member3.jpg">
                </div>
                <h4 class="membername">JULIETTE LIGHT</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook-white.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter-white.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram-white.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble-white.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_3">
        	<!--start member-->
            <div class="member red fade-left animate4 opacitybg">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member4.jpg">
                </div>
                <h4 class="membername">MARK SPITCH</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook-white.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter-white.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram-white.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble-white.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        

    </div>
    <!--end container--> 
    
</section>
<!--end section-->

<!--start page-->
<section class="sectionhome">

	<!--start container-->
    <div class="container clearfix">
    
		<div class="grid_12">
			<div class="dividerheight40"></div>
			<div class="titlesection orange">
				<h2 class="titlewithborder"><span>3 COLUMNS</span></h2>
			</div>  
		</div>
	
        <div class="grid_4">
        	<!--start member-->
            <div class="member red fade-left animate1">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member1.jpg">
                </div>
                <h4 class="membername">JANE MC DOE</h4>
                <p class="memberposition"><i>Owner</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_4">
        	<!--start member-->
            <div class="member orange fade-left animate2">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member2.jpg">
                </div>
                <h4 class="membername">NICK HOPE</h4>
                <p class="memberposition"><i>Cofounder</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
        
        <div class="grid_4">
        	<!--start member-->
            <div class="member yellow fade-left animate3">
                <div class="imgmember">
                	<img alt="" class="opacity" src="img/team/member/member3.jpg">
                </div>
                <h4 class="membername">JULIETTE LIGHT</h4>
                <p class="memberposition"><i>Sales Agent</i></p>
                <p class="memberdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut cursus eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                <div class="socialmember">
                	<ul>
                    	<li><a title="Facebook" class="tooltip" href="#"><img alt="" src="img/team/facebook.png"></a></li>
                        <li><a title="Twitter" class="tooltip" href="#"><img alt="" src="img/team/twitter.png"></a></li>
                        <li><a title="Instagram" class="tooltip" href="#"><img alt="" src="img/team/instagram.png"></a></li>
                        <li><a title="Dribble" class="tooltip" href="#"><img alt="" src="img/team/dribble.png"></a></li>
                    </ul>
                </div>
        	</div>
            <!--end member-->
        </div>
      
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>


<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			$('.header-page').parallax("100%", 0.1);
			$('.sectionparallax').parallax("100%", 0.1);
		});
		//end parallax
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		

		/* ]]> */
	</script>
    
</body>  
</html>