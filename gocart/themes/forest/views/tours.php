<?php include "include/header.php"; ?>

<section class="header-page fade-up header-page-tours">
	<div class="bounce-in animate4"><h2 class="header-pagetitle">AMAZING TOURS</h2></div>
</section>

<div class="divider"><span></span></div>

<!--start page-->
<section id="internalpage">

	<!--start container-->
    <div class="container clearfix">
    

		<div class="grid_6">
        	<!--start archivetour-->
            <div class="archivetour yellow fade-left animate1">
                
                <!--left-->
                <div class="leftarchivetour">
                
                    <a href="single-project.php"><img alt="" class="imgleftarchivetour opacity" src="img/tours/photos/img1.jpg"></a>
                
                    <div class="pricetitleleftarchivetour">
                        
                        <div class="priceleftarchivetour">
                            <p>1000 $</p>
                        </div>
                        <p class="titleleftarchivetour">For person</p>
                    </div>
                
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivetour">
                
                    <div class="titledayarchivetour">
                        <a href="single-project.php"><p class="titlearchivetour">BRAZIL</p></a>
                    
                        <div class="dayarchivetour">
                            <p>15</p>
                            <span>DAYS</span>
                        </div>
                    
                    </div>
                    
                    <div class="descriptioniconsarchivetour">
                    
                        <p class="descriptionarchivetour">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh.</p>
                        <p class="iconsarchivetour">
                        
                            <a title="Car" class="tooltip" href="#"><img alt="" src="img/tours/icon/car.png"></a>
                            <a title="Fly and Drive" class="tooltip" href="#"><img alt="" src="img/tours/icon/plane.png"></a>
                            <a title="Sun" class="tooltip" href="#"><img alt="" src="img/tours/icon/sun.png"></a>
                            <a title="Insurance included" class="tooltip" href="#"><img alt="" src="img/tours/icon/insurance.png"></a>
							<a title="Nature" class="tooltip" href="#"><img alt="" src="img/tours/icon/nature.png"></a>
                        
                        </p>
                    
                    </div>
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archivetour-->
        </div>
        
        <div class="grid_6">
        	<!--start archivetour-->
            <div class="archivetour orange fade-right animate1">
                
                <!--left-->
                <div class="leftarchivetour">
                
                    <a href="single-project.php"><img alt="" class="imgleftarchivetour opacity" src="img/tours/photos/img2.jpg"></a>
                
                    <div class="pricetitleleftarchivetour">
                        
                        <div class="priceleftarchivetour">
                            <p>1000 $</p>
                        </div>
                        <p class="titleleftarchivetour">Bryce canyon tour</p>
                    </div>
                
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivetour">
                
                    <div class="titledayarchivetour">
                        <a href="single-project.php"><p class="titlearchivetour">WEST COST</p></a>
                    
                        <div class="dayarchivetour">
                            <p>10</p>
                        <span>DAYS</span>
                    
                    </div>
                    
                    </div>
                    
                    <div class="descriptioniconsarchivetour">
                    
                        <p class="descriptionarchivetour">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh.</p>
                        <p class="iconsarchivetour">
                        
                            <a title="Car" class="tooltip" href="#"><img alt="" src="img/tours/icon/car.png"></a>
                            <a title="Fly and Drive" class="tooltip" href="#"><img alt="" src="img/tours/icon/plane.png"></a>
                            <a title="Sun" class="tooltip" href="#"><img alt="" src="img/tours/icon/sun.png"></a>
                            <a title="Insurance included" class="tooltip" href="#"><img alt="" src="img/tours/icon/insurance.png"></a>
							<a title="Nature" class="tooltip" href="#"><img alt="" src="img/tours/icon/nature.png"></a>
                        
                        </p>
                    
                    </div>
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archivetour-->
        </div>
        
        <div class="grid_6">
        	<!--start archivetour-->
            <div class="archivetour red fade-left animate1">
                
                <!--left-->
                <div class="leftarchivetour">
                
                    <a href="single-project.php"><img alt="" class="imgleftarchivetour opacity" src="img/tours/photos/img3.jpg"></a>
                
                    <div class="pricetitleleftarchivetour">
                        
                        <div class="priceleftarchivetour">
                            <p>700 $</p>
                        </div>
                        <p class="titleleftarchivetour">Amazing tour</p>
                    </div>
                
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivetour">
                
                    <div class="titledayarchivetour">
                        <a href="single-project.php"><p class="titlearchivetour">TURKEY</p></a>
                    
                        <div class="dayarchivetour">
                            <p>10</p>
                        <span>DAYS</span>
                    
                    </div>
                    
                    </div>
                    
                    <div class="descriptioniconsarchivetour">
                    
                        <p class="descriptionarchivetour">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh.</p>
                        <p class="iconsarchivetour">
                        
                            <a title="Car" class="tooltip" href="#"><img alt="" src="img/tours/icon/car.png"></a>
                            <a title="Fly and Drive" class="tooltip" href="#"><img alt="" src="img/tours/icon/plane.png"></a>
                            <a title="Sun" class="tooltip" href="#"><img alt="" src="img/tours/icon/sun.png"></a>
                            <a title="Insurance included" class="tooltip" href="#"><img alt="" src="img/tours/icon/insurance.png"></a>
							<a title="Nature" class="tooltip" href="#"><img alt="" src="img/tours/icon/nature.png"></a>
                        
                        </p>
                    
                    </div>
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archivetour-->
        </div>
        
        <div class="grid_6">
        	<!--start archivetour-->
            <div class="archivetour green fade-right animate1">
                
                <!--left-->
                <div class="leftarchivetour">
                
                    <a href="single-project.php"><img alt="" class="imgleftarchivetour opacity" src="img/tours/photos/img4.jpg"></a>
                
                    <div class="pricetitleleftarchivetour">
                        
                        <div class="priceleftarchivetour">
                            <p>900 $</p>
                        </div>
                        <p class="titleleftarchivetour">Adventure travel</p>
                    </div>
                
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivetour">
                
                    <div class="titledayarchivetour">
                        <a href="single-project.php"><p class="titlearchivetour">ITALY</p></a>
                    
                        <div class="dayarchivetour">
                            <p>7</p>
                        <span>DAYS</span>
                    
                    </div>
                    
                    </div>
                    
                    <div class="descriptioniconsarchivetour">
                    
                        <p class="descriptionarchivetour">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh.</p>
                        <p class="iconsarchivetour">
                        
                            <a title="Car" class="tooltip" href="#"><img alt="" src="img/tours/icon/car.png"></a>
                            <a title="Fly and Drive" class="tooltip" href="#"><img alt="" src="img/tours/icon/plane.png"></a>
                            <a title="Sun" class="tooltip" href="#"><img alt="" src="img/tours/icon/sun.png"></a>
                            <a title="Insurance included" class="tooltip" href="#"><img alt="" src="img/tours/icon/insurance.png"></a>
							<a title="Nature" class="tooltip" href="#"><img alt="" src="img/tours/icon/nature.png"></a>
                        
                        </p>
                    
                    </div>
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archivetour-->
        </div>
        
        <div class="grid_6">
        	<!--start archivetour-->
            <div class="archivetour blue fade-left animate1">
                
                <!--left-->
                <div class="leftarchivetour">
                
                    <a href="single-project.php"><img alt="" class="imgleftarchivetour opacity" src="img/tours/photos/img5.jpg"></a>
                
                    <div class="pricetitleleftarchivetour">
                        
                        <div class="priceleftarchivetour">
                            <p>1000 $</p>
                        </div>
                        <p class="titleleftarchivetour">Relax tour</p>
                    </div>
                
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivetour">
                
                    <div class="titledayarchivetour">
                        <a href="single-project.php"><p class="titlearchivetour">ALASKA</p></a>
                    
                        <div class="dayarchivetour">
                            <p>10</p>
                        <span>DAYS</span>
                    
                    </div>
                    
                    </div>
                    
                    <div class="descriptioniconsarchivetour">
                    
                        <p class="descriptionarchivetour">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh.</p>
                        <p class="iconsarchivetour">
                        
                            <a title="Car" class="tooltip" href="#"><img alt="" src="img/tours/icon/car.png"></a>
                            <a title="Fly and Drive" class="tooltip" href="#"><img alt="" src="img/tours/icon/plane.png"></a>
                            <a title="Sun" class="tooltip" href="#"><img alt="" src="img/tours/icon/sun.png"></a>
                            <a title="Insurance included" class="tooltip" href="#"><img alt="" src="img/tours/icon/insurance.png"></a>
							<a title="Nature" class="tooltip" href="#"><img alt="" src="img/tours/icon/nature.png"></a>
                        
                        </p>
                    
                    </div>
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archivetour-->
        </div>
        
        <div class="grid_6">
        	<!--start archivetour-->
            <div class="archivetour violet fade-right animate1">
                
                <!--left-->
                <div class="leftarchivetour">
                
                    <a href="single-project.php"><img alt="" class="imgleftarchivetour opacity" src="img/tours/photos/img6.jpg"></a>
                
                    <div class="pricetitleleftarchivetour">
                        
                        <div class="priceleftarchivetour">
                            <p>500 $</p>
                        </div>
                        <p class="titleleftarchivetour">For person</p>
                    </div>
                
                
                </div>
                <!--end left-->
                
                <!--right-->
                <div class="rightarchivetour">
                
                    <div class="titledayarchivetour">
                        <a href="single-project.php"><p class="titlearchivetour">FRANCE</p></a>
                    
                        <div class="dayarchivetour">
                            <p>9</p>
                        <span>DAYS</span>
                    
                    </div>
                    
                    </div>
                    
                    <div class="descriptioniconsarchivetour">
                    
                        <p class="descriptionarchivetour">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin cursus, elit vitae fermentum hendrerit, neque erat fringilla nibh.</p>
                        <p class="iconsarchivetour">
                        
                            <a title="Car" class="tooltip" href="#"><img alt="" src="img/tours/icon/car.png"></a>
                            <a title="Fly and Drive" class="tooltip" href="#"><img alt="" src="img/tours/icon/plane.png"></a>
                            <a title="Sun" class="tooltip" href="#"><img alt="" src="img/tours/icon/sun.png"></a>
                            <a title="Insurance included" class="tooltip" href="#"><img alt="" src="img/tours/icon/insurance.png"></a>
							<a title="Nature" class="tooltip" href="#"><img alt="" src="img/tours/icon/nature.png"></a>
                        
                        </p>
                    
                    </div>
                
                </div>
                <!--end right-->
                   
            </div>
            <!--end archivetour-->
        </div>
        
    
    </div>
    <!--end container--> 
    
</section>
<!--end internal page-->

<div class="divider"><span></span></div>

<?php include "include/footer.php"; ?>

	<!--Start js-->    
    <script src="js/jquery.min.js"></script> <!--Jquery-->
    <script src="js/jquery-ui.js"></script> <!--Jquery UI-->
    <script src="js/excanvas.js"></script> <!--canvas need for ie-->
    <script src="js/scroolto.js"></script> <!--Scrool To-->
    <script src="js/jquery.nicescroll.min.js"></script> <!--Nice Scroll-->
    <script src="js/jquery.inview.min.js"></script> <!--inview-->
	<script src="js/menu/hoverIntent.js"></script> <!--superfish-->
	<script src="js/menu/superfish.min.js"></script> <!--superfish-->
    <script src="js/menu/tinynav.min.js"></script> <!--tinynav-->
    <script src="js/jquery.parallax-1.1.3.js"></script> <!--parallax-->
	<script src="js/twitter/jquery.twitterfeed.min.js"></script> <!--twitter-->
    <script src="js/settings.js"></script> <!--settings-->
    <!--End js-->
	
	
	<script type='text/javascript'>
		/* <![CDATA[ */
		

		//start parallax
		jQuery(document).ready(function() {
			
			$('.header-page').parallax("100%", 0.1);

		});
		//end parallax
		
		
		//start tooltip
		$(document).ready(function() {
			$( ".tooltip" ).tooltip({ position: { my: "top+0 top-75", at: "center center" } });
		});
		//end tooltip
		

		/* ]]> */
	</script>
    
</body>  
</html>